"use strict";
const express = require("express");
const http = require("http");
const path = require("path");
const session = require("express-session");
const bodyParser = require("body-parser");
const errorHandler = require("errorhandler");
const cookieParser = require("cookie-parser");
const favicon = require("serve-favicon");
const logger = require("morgan");
const methodOverride = require("method-override");
const fs = require("fs");
const MongoStore = require('connect-mongo')(session);
var WechatAPI = require('wechat-api');
var Wechat = require('wechat');
var app = express();
// all environments
app.set('port', process.env.PORT || 3000);
process.env.RootPath = process.env.RootPath || __dirname + "/";
process.env.TempPath = process.env.TempPath || "temp/";
process.env.PicPath = process.env.PicPath || "imgs/";
process.env.DocPath = process.env.DocPath || "docs/";
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
app.use(favicon(__dirname + '/client/wechat/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(cookieParser());
var sessionurl = process.env.SESSIONURL || "mongodb://127.0.0.1/session";
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: "anniexionggtscloudcc",
    store: new MongoStore({ url: sessionurl })
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// import init = require('./server/modules/init');
// init();
var routers = JSON.parse(fs.readFileSync(__dirname + "/router.json", "utf8"));
app.use("/libs", express.static(path.join(__dirname, 'client/libs')));
app.use(express.static(path.join(__dirname, 'client/wechat')));
app.use(express.static(path.join(__dirname, 'client/login')));
// routers.public.forEach(router => {
//     var modulefile = require(router.file);
//     app.use(router.url, modulefile);
// })
//登录注册
//import logon = require('./server/common/logon');
//app.use(logon);
// import wechatmenu = require('./server/common/wechatmenus');
// app.use(wechatmenu);
//微信内容
const wechat = require('./server/public/wechat');
app.use(wechat);
//var api =new WechatAPI(app_id,app_secret);
// api.updateRemark('open_id', 'remarked', function (err, data, res) {
//   // TODO
// });
//import stylus = require('stylus');
//app.use(stylus.middleware(path.join(__dirname, 'public')));
//app.use(express.static(path.join(__dirname, 'client/wechat')));
//待测试这段代码是否生效
app.use((req, res, next) => {
    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Pragma', 'no-cache');
    res.setHeader('Expires', '-1');
    next();
});
routers.private.forEach(router => {
    var modulefile = require(router.file);
    app.use(router.url, modulefile);
});
//development only
if ('development' == app.get('env')) {
    routers.development.forEach(router => {
        var modulefile = require(router.file);
        app.use(router.url, modulefile);
    });
}
//app.use(util.WebixError);
if ('development' == app.get('env')) {
    app.use(errorHandler());
}
http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
//# sourceMappingURL=app.js.map