/******login js******/
$(document).ready(function(){
	
	//回车时登录事件
	$(document).keydown(function(event){
		 var keyCode = event.which ? event.which : event.keyCode;
		 //回车
		 if(keyCode==13){
			 checkUser();
		 }
	 });
	
	   if(errorcount>3){
		    middleblockheigt = 400;
	   }else{
			middleblockheigt = 330;
	   }
		var availheight = document.documentElement.clientHeight;
		if(availheight<550){
			availheight = 550;
		}
		var topheight = (availheight-middleblockheigt)/2-10;
		$("#middleblock").css({"margin-top":topheight+"px"});
		//错误信息提示
		if(!isBlank(errormsg)){
			 $.fn.consoleAlter(errormsg,"error",2000);
		}
	});
	
	//登录
	function checkUser(){
		if($(".login-btn-red").hasClass("disable"))return false;
		var username = $("#username").val();
		if(isBlank(username)){
			  $.fn.consoleAlter("登录账户不可为空","error",2000);
			  return;
		}
		var psw = $("#password").val();
		if(isBlank(psw)){
			  $.fn.consoleAlter("登录密码不可为空","error",2000);
			  return;
		}
		
		if(errorcount>3){
			//验证码
			var checkcode = $("#checkcodeinput").val();
			if(isBlank(checkcode)){
				  $.fn.consoleAlter("验证码不可为空","error",2000);
				  return;
			}
		}
		$(".login-btn-red").addClass("disable");
		//var url = document.getElementsByTagName("base")[0].getAttribute("href")+"login/check";
		//document.forms[0].action=url;
		document.forms[0].submit();
	}