//页面提示的消息
/**
 * //页面提示的消息，确认对话框
 * msg 是提示信息
 * type 分为 info,error,warning,ok
 * @param $
 */
(function($){
	
  //通用加载loding框
  $.fn.consoleLoading = {
		  show:function(msg){
			  if(isBlank(msg)){
				  msg = "请稍后...";
			  }
			  var html = " <div id='consoletool-loading' style='width:100%;height:100px;margin: 0; padding: 0;position:fixed;z-index:1000;' > ";
			  html+=" 	<div  style='width: 100%;height: 100%;margin: 0; padding: 0;position: fixed;background: #333;z-index: -1;filter: alpha(opacity=70);-moz-opacity: 0.70;opacity: 0.70;'> ";
			  html+=" </div> ";
			  html+=" 	<div id='consoletool-loading-bar'  style='margin:0 auto;width:400px;height:30px;line-height:30px;color:#fff;text-align:center;'> ";
			  html+=" 			<img src='static/img/loader.gif' style='width:20px;height:20px;border:0;vertical-align:middle;'  /> ";
			  html+=" 			<span id='consoletool-loadingmsg' style='height:30px;'>请稍后...</span> ";
			  html+=" 	</div> ";
			  html+=" </div> ";
			  //先删除，后加入
			  $("body").remove("#consoletool-loading");
			  $("body").append(html);
			  $("#consoletool-loadingmsg").text(msg);
			  setBoxMiddleTop("#consoletool-loading-bar",30);
		 },
		 close:function(){
			 $("#consoletool-loading").remove();
		 }
  };
	
  //通用提示框
  $.fn.consoleAlter = function(msg,type,time) {
	 var alerthtml ="<div id='consoleAlterBlock' style='width:auto; padding:20px 20px; background:#fff; border:1px solid  rgb(185, 185, 185);border-radius:4px;-moz-box-shadow:1px 3px 12px #bbb;  -webkit-box-shadow:1px 3px 12px #bbb; "; 
	  alerthtml+="box-shadow:1px 3px 12px #bbb;  color:#000; display:none; position: fixed; left:50%; top:50%;z-index:999;'>                                                                              ";
	  alerthtml+="    <div style='width:24px; height:24px; line-height:24px; float:left;'>                                                                                                      ";
	  alerthtml+="    	<div style='width:24px; height:24px; background:url("+document.getElementsByTagName("base")[0].getAttribute("href")+"static/img/waring-icons/"+type+"24.png)'>                                                                                                                 ";
	  alerthtml+="    	</div>                                                                                                                                                                  ";
	  alerthtml+="    </div>                                                                                                                                                                    ";
	  alerthtml+="    <div style='width:auto; height:24px; margin-left:10px;line-height:24px; text-align:center;float:left;font-size:14px;'>                                                   ";
	  alerthtml+= msg;
	  alerthtml+="    </div>                                                                                                                                                                    ";
	  alerthtml+="</div>                                                                                                                                                                        ";
	  //先删除，后加入
	  $("body").remove("#consoleAlterBlock");
	  $("body").append(alerthtml);
	  //居中
	  var validWidth = window.screen.availWidth;
	  var validHeight = window.screen.availHeight;
	  $("#consoleAlterBlock").css("left",(validWidth-$("#consoleAlterBlock").width())/2);
	  $("#consoleAlterBlock").css("top",(validHeight-$("#consoleAlterBlock").width())/2);
	  $("#consoleAlterBlock").fadeIn(500);
	  setTimeout(hideAndRemoveAlterBlock,time);
  };
  //自定义确认对话框,警告框
  $.fn.MsgBox = {
	        Alert: function (title, msg) {
	            GenerateHtml("alert", title, msg);
	            btnOk();  //alert只是弹出消息，因此没必要用到回调函数callback
	            btnNo();
	        },
	        Confirm: function (title, msg, callback,cancel) {
	            GenerateHtml("confirm", title, msg);
	            btnOk(callback);
	            btnNo(cancel);
	        }
  };
  var GenerateHtml = function (type, title, msg) {
      var _html = "";
      _html += '<div id="mb_box"></div><div id="mb_con"><span id="mb_tit">' + title + '</span>';
      _html += '<a id="mb_ico"></a><div id="mb_msg">' + msg + '</div><div id="mb_btnbox">';
      if (type == "alert") {
          _html += '<input id="mb_btn_ok" type="button" value="确定" />';
      }
      if (type == "confirm") {
          _html += '<input id="mb_btn_ok" type="button" value="确定" />';
          _html += '<input id="mb_btn_no" type="button" value="取消" />';
      }
      _html += '</div></div>';
      //必须先将_html添加到body，再设置Css样式
      $("body").append(_html); GenerateCss();
  };
  //生成Css
    var GenerateCss = function () {
        $("#mb_box").css({ width: '100%', height: '100%', zIndex: '99999', position: 'fixed',
            filter: 'Alpha(opacity=60)', backgroundColor: 'black', top: '0', left: '0', opacity: '0.6'
        });
        $("#mb_con").css({ zIndex: '999999', width: '400px', position: 'fixed',
            backgroundColor: '#fff'
        });
        $("#mb_tit").css({ display: 'block', fontSize: '14px', color: '#fff', padding: '10px 15px',
            backgroundColor: 'rgb(57,58,63)', 
            fontWeight: 'bold'
        });
        $("#mb_msg").css({ padding: '20px', lineHeight: '20px',
            borderBottom: '1px dashed #DDD', fontSize: '13px'
        });
        $("#mb_ico").css({ display: 'block', position: 'absolute', right: '10px', top: '9px',
            width: '16px', height: '16px', textAlign: 'center',
            lineHeight: '16px', cursor: 'pointer', fontFamily: '微软雅黑', 
            backgroundImage:"url('static/img/close16x16.png')"
        });
        $("#mb_btnbox").css({ margin: '15px 0 15px 0', textAlign: 'center' });
        $("#mb_btn_ok,#mb_btn_no").css({ width: '85px', height: '30px', color: 'white', border: 'none' ,cursor:'pointer'});
        $("#mb_btn_ok").css({ backgroundColor: '#168bbb' });
        $("#mb_btn_no").css({ backgroundColor: 'gray', marginLeft: '20px' });

        var _widht = document.documentElement.clientWidth;  //屏幕宽
        var _height = document.documentElement.clientHeight; //屏幕高
        var boxWidth = $("#mb_con").width();
        var boxHeight = $("#mb_con").height();
        //让提示框居中
        $("#mb_con").css({ top: (_height - boxHeight) / 2 + "px", left: (_widht - boxWidth) / 2 + "px" });
    };
    //确定按钮事件
    var btnOk = function (callback) {
        $("#mb_btn_ok").click(function () {
            $("#mb_box,#mb_con").remove();
            if (typeof (callback) == 'function') {
                callback();
            }
        });
    };
    //取消按钮事件
    var btnNo = function (cancel) {
        $("#mb_btn_no,#mb_ico").click(function () {
            $("#mb_box,#mb_con").remove();
            if (typeof (cancel) == 'function') {
            	cancel();
            }
        });
    };

    
    //展示图片
    $.fn.consoleImgBox = function(imgurl){
    	var imghtml = "";
    	imghtml +="<div style='width:100%;height:100%;position:fixed;' id='awspaas-imgbox-tool'>  ";
    	imghtml +=" <div style='width:100%;height:100%;position:absolute;background: #333;z-index:-1;filter: alpha(opacity=30);-moz-opacity: 0.30;opacity: 0.30;'>  ";
    	imghtml +=" </div>";
    	imghtml +=" <div id='awspaas-imgboxouter' style='width:auto;height:auto;margin:0 auto;border:1px solid rgb(82, 80, 80);position:relative;webkit-box-shadow: 0px 0px 20px #666;-moz-box-shadow: 0px 0px 20px #666;box-shadow: 0px 0px 20px #666;'>  ";
    	imghtml +="	<img id='awspaas-imgbox' src=\""+imgurl+"\" onload='initImgBoxTool();'  />  ";
    	imghtml +="	<img src='static/img/close_red24x24.png' style='position:absolute;top:-12px;right:-12px;width:30px;height:30px;cursor:pointer;'  onclick='javascript:$(\"#awspaas-imgbox-tool\").remove();' /> ";
    	imghtml +=" </div>";
    	imghtml +="</div> ";
    	$("body").append(imghtml);
     };
    
})(jQuery);

//配合imgbox
function initImgBoxTool(){
	//获得浏览器窗口的高度
	var availheight = document.documentElement.clientHeight;
	var oldheight = $("#awspaas-imgbox").height();
	var oldwidth = $("#awspaas-imgbox").width();
	var newheight = oldheight;
	if(availheight*0.9<oldheight){
		newheight = availheight*0.9;
	}
	var newwidth = (newheight*oldwidth)/oldheight;
	/**设置面板的宽高**/
	var availheight = document.documentElement.clientHeight;
	var margintop = (availheight - newheight)/2;
	$("#awspaas-imgboxouter").css({"height":newheight+"px","width":newwidth+"px","margin-top":margintop+"px"});
	$("#awspaas-imgbox").css({"height":newheight+"px","width":newwidth+"px"});
}


//删除
function hideAndRemoveAlterBlock(){
	$("#consoleAlterBlock").fadeOut(500);
	$("#consoleAlterBlock").remove();
}

/************************string相关操作**************************/
//字符串去两边的空格
String.prototype.trim = function(){ 
	return this.replace(/(^\s*)|(\s*$)/g, ""); 
};
String.prototype.ltrim = function(){ 
	return this.replace(/(^\s*)/g, ""); 
};
String.prototype.rtrim = function(){ 
	return this.replace(/(\s*$)/g, ""); 
};
//js endwith方法
String.prototype.endWith=function(str){
	if(str==null||str==""||this.length==0||str.length>this.length)
	  return false;
	if(this.substring(this.length-str.length)==str)
	  return true;
	else
	  return false;
	return true;
};
//js startwith方法
String.prototype.startWith=function(str){
	if(str==null||str==""||this.length==0||str.length>this.length)
	  return false;
	if(this.substr(0,str.length)==str)
	  return true;
	else
	  return false;
	return true;
};
//js 获得字符串的字节长度
String.prototype.getBytesLength = function(){   
    var totalLength = 0;     
    var charCode;  
    for (var i = 0; i < this.length; i++) {  
        charCode = this.charCodeAt(i);  
        if (charCode < 0x007f)  {     
            totalLength++;     
        } else if ((0x0080 <= charCode) && (charCode <= 0x07ff))  {     
            totalLength += 2;     
        } else if ((0x0800 <= charCode) && (charCode <= 0xffff))  {     
            totalLength += 3;   
        } else{  
            totalLength += 4;   
        }          
    }  
    return totalLength;   
}
/**
 * js实现replaceAll
 * @param reallyDo 要替换的字符串old
 * @param replaceWith 替换的字符串new
 * @param ignoreCase
 * @returns
 */
String.prototype.replaceAll = function(reallyDo, replaceWith, ignoreCase) {  
    if (!RegExp.prototype.isPrototypeOf(reallyDo)) {  
        return this.replace(new RegExp(reallyDo, (ignoreCase ? "gi": "g")), replaceWith);  
    } else {  
        return this.replace(reallyDo, replaceWith);  
    }  
};  
Date.prototype.format =function(format)
{
	var o = {
	"M+" : this.getMonth()+1, //month
	"d+" : this.getDate(), //day
	"h+" : this.getHours(), //hour
	"m+" : this.getMinutes(), //minute
	"s+" : this.getSeconds(), //second
	"q+" : Math.floor((this.getMonth()+3)/3), //quarter
	"S" : this.getMilliseconds() //millisecond
	};
	if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
	(this.getFullYear()+"").substr(4- RegExp.$1.length));
	for(var k in o)if(new RegExp("("+ k +")").test(format))
	format = format.replace(RegExp.$1,
	RegExp.$1.length==1? o[k] :
	("00"+ o[k]).substr((""+ o[k]).length));
	return format;
};
/*判断一个值是否为空*/
function isBlank(val){
	if(typeof val == "undefined" || val== null){
		return true;
	}
	val = val.trim();
	if(val.length<1){
		return true;	
	} 
	return false;
}


/**
 * 从转义符获得html标签字符串
 * @param str
 * @return
 */
function getHtmlFormEscapeSquence(str){
	str = str.replaceAll("&amp;", "&")
	   .replaceAll("&lt;", "<")
	   .replaceAll("&gt;", ">")
	   .replaceAll("&quot;", "\"")
	   .replaceAll("&amp;","&")
	   .replaceAll("&nbsp;", " ")
	   .replaceAll("&copy;", "©")
	   .replaceAll("&reg;","®")
	   .replaceAll("&apos;", "'");
	return str;
}

/**
 * 对html标签字符串进行转义
 * @param str
 * @return
 */
function getEscapeSquenceFormHtml(str){
	str = str.replaceAll("&amp;", "&")
	   .replaceAll("<","&lt;")
	   .replaceAll(">","&gt;")
	   .replaceAll("\"","&quot;")
	   .replaceAll("&","&amp;")
	   .replaceAll("©","&copy;")
	   .replaceAll(" ", "&nbsp;")
	   .replaceAll("®","&reg;")
	   .replaceAll("'","&apos;");
	return str;
}

/**
 * 清除字符串中的html标签
 * @param str
 * @returns
 */
function removeHtmlTag(str){
	str = str.replace(/\<.[^<>]*\>/g,"");
	return str;
}
/**
 * 按钮失效
 * @param containerId
 */
function disableBtn(containerId){
	if (containerId && typeof(containerId) == "string") {
		$("#"+containerId).find("button, .button, input[type='button']").attr("disabled","true");
		$("#"+containerId).find("button, .button, input[type='button']").addClass("disable");
	} else if (containerId) {
		$(containerId).find("button, .button, input[type='button']").attr("disabled","true");
		$(containerId).find("button, .button, input[type='button']").addClass("disable");
	} else {
		$("button, .button, input[type='button']").attr("disabled","true");
		$("button, .button, input[type='button']").addClass("disable");
	}
}
/**
 * 按钮生效
 * @param containerId
 */
function enableBtn(containerId) {
	if (containerId) {
		$("#"+containerId).find("button, .button, input[type='button']").removeAttr("disabled");
		$("#"+containerId).find("button, .button, input[type='button']").removeClass("disable");
	} else {
		$("button, .button, input[type='button']").removeAttr("disabled");
		$("button, .button, input[type='button']").removeClass("disable");
	}
}
/**
 * 到登录页面
 */
function goLoginPage(){
	window.location.href=document.getElementsByTagName("base")[0].getAttribute("href")+"login";
}
/**
*模态对话框垂直显示位置
*/
function openModal(id){
	$("#"+id).show();
	var windowHeight = document.documentElement.clientHeight;
	var modalHeight = $("#"+id+">.console-modal").height();
	var modalWidth = $("#"+id+">.console-modal").width();
	if (windowHeight > modalHeight) {
		$("#"+id+">.console-modal").css({
			"top":(windowHeight - modalHeight) / 2 + "px",
			"left":"50%",
			"marginLeft":-modalWidth/2
		});
	}else{
		$("#"+id+">.console-modal").css({
			"top":"100px",
			"left":"50%",
			"marginLeft":-modalWidth/2
		});
	}
}

/**
 * 四舍五入，保留指定位数的小数
 * @param num 数字
 * @param scale 精度
 */
function mathRound(num,scale){
	if(isNaN(num)){
		return 0.00;
	}
	var f_x = parseFloat(num);
	var snum = Math.pow(10,scale);
	f_x = Math.round(f_x*snum)/snum;
	return f_x;
}

/**
 * 设置面板垂直居中
 * @param selector 面板的id或类等选择器，如id，则#id，如果是类则.class形式
 * @param boxHeight 面板的高度
 */
function setBoxMiddleTop(selector,boxHeight){
	var dheight = $(window).height();
	if(dheight>boxHeight){
		var top = (dheight-boxHeight)/2;
		$(selector).css({"margin-top":top+"px"});
	}
}

//检查密码强度
function checkPwdStrength(gets){
	var length=gets.length;
	//密码长度6-20之间
	if(length < 6 || length > 20)return false;
	var e = 0;//密码强度度指标，长度4，由1，2，4，8或运算得到
	for(var c=0;c<length;c++){
		var charVal = gets.charCodeAt(c);
		if(charVal >= 48 && charVal <= 57){
			//数字
			e |= 1;
		}else{
			if(charVal >= 65 && charVal <=90){
				//大写字母
				e |= 2;
			}else{
				if(charVal >= 97 && charVal <= 122){
					//小写字母
					e |= 4;
				}else{
					//特殊字符
					e |= 8;
				}
			}
		}
	}
	//将e量化，大于60（数组，大、小写字母，特殊字符中的三种以上-75%）为强密码
	var d=0;
	for(var c=0;c<4;c++){
		if(e&1){
			d++;
		}
		e>>>=1;
	}
	d = d/4*100;
	if(d <= 60){
		return false;
	}else{
		return true;
	}
}