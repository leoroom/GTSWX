var allvendors;
function loadVendors() {
    $.get('/sign', {}, function (data) {
        var vendors = data;
        allvendors = vendors;
        $('div .tile-container.bg-darkCobalt').empty();
        vendors.forEach(function (element) {
            if (element.vendor) {
                var vendorHTML = getVendorHTML(element);
                $('div .tile-container.bg-darkCobalt').append(vendorHTML);
            }
        }, this);
    })
};
function getVendorHTML(vendor) {
    var vendorHTML = "";
    if (vendor.status == "未签到") {
        vendorHTML += ' <div class="tile-wide bg-orange fg-white" onClick="onSign(this);"  ' + 'id=' + vendor.id + ' >';
        vendorHTML += '  <div class="tile-content slide-left-2">';
        vendorHTML += '  <div class="slide" style="text-align:center;">';
        vendorHTML += '  </br></br>';
        vendorHTML += '  <p>' + vendor.vendor + '</p>';
        vendorHTML += '  <p>' + vendor.users.map(function (user) { return user.name }).join(" ") + '</p>';
        vendorHTML += '  </div>';
        vendorHTML += '  <div class="slide-over padding10 element-selected"  style="text-align:center;">';
        vendorHTML += '  按下或者点击完成签到</br></br>';
        vendorHTML += '  <p>' + vendor.vendor + '</p>';
        vendorHTML += '  </div>';
        vendorHTML += '  <div class="tile-label" style="display:block">' + vendor.status + '</div>';
        vendorHTML += '   </div>';
        vendorHTML += '   </div>';
        return vendorHTML;
    }
    else {
        vendorHTML += ' <div class="tile-wide bg-lightGreen fg-white" onClick="onSign(this);"  ' + 'id=' + vendor.id + ' >';
        vendorHTML += '  <div class="tile-content slide-left">';
        vendorHTML += '  <div class="slide element-selected" style="text-align:center;">';
        vendorHTML += '  </br></br>';
        vendorHTML += '  <p>' + vendor.vendor + '</p>';
        vendorHTML += '  <p>' + _.filter(vendor.users,{sign:true}).map(function (user) { return user.name }).join(" ") + '</p>';
        vendorHTML += '  </div>';
        vendorHTML += '  <div class="slide-over padding10 element-selected"  style="text-align:center;display:none">';
        vendorHTML += '  按下或者点击完成签到</br></br>';
        vendorHTML += '  <p>' + vendor.vendor + '</p>';
        vendorHTML += '  </div>';
        vendorHTML += '  <div class="tile-label" style="display:block">' + vendor.status + '</div>';
        vendorHTML += '   </div>';
        vendorHTML += '   </div>';
        return vendorHTML;
    }

};
var popWin = {
    id: "popWin", view: "window", modal: true, move: true, position: "center", head: {
        view: "toolbar", cols: [

            { view: "label", label: "人员信息", id: "title" },
            {},
            {
                view: "icon", icon: "close", align: "left", click: function () {
                    webix.ajax().post("/sign/" + $$("detailtable").config.vendorid, {data:$$('detailtable').data.find({ch:true})}, function (text, res) {
                        var data = res.json();
                        if (data.status) {
                            var root =$$("detailtable").config.element;
                            root.removeClass("bg-orange").addClass("bg-lightGreen");

                            var div = $(root.children(":first"));
                            div.attr('class', 'tile-content slide-left');

                            div.find('.slide').addClass('element-selected');
                            div.find('.slide-over').css('display', 'none');
                            //  var divSliderOver = $(div.children("div").children(".slide-over"));
                            // divSliderOver.css('display', 'none');

                            div.find('.tile-label').text('已签到');
                        }
                        $$('popWin').hide();
                    });
                   
                }
            }
        ]
    }, height: 400, width: 600,
    body: {
        rows: [
            {
                view: "datatable", id: "detailtable", editable: false, rowHeight: 50,
                columns: [
                    { id: "ch", header: { content: "masterCheckbox" }, template: "{common.checkbox()}" },
                    { id: "name", header: "姓名", width: 100, editor: "text" },
                    { id: "role", header: "职位", fillspace: true, editor: "text" }
                ], on: {
                    onItemClick: function (id) {
                        var item = this.getItem(id);
                        item.ch = !item.ch;
                        this.updateItem(item.id, item);
                    }
                },
                save: { autoupdate: true }
            }
        ]
    }
};
function onSign(element) {
    var id = $(element).attr('id');
    var win;
    if (!$$('popWin')) { win = webix.ui(popWin); }
    else win = $$('popWin');
    var vendor = _.find(allvendors, { id: id });
    $$("title").setValue(vendor.vendor)
    $$("detailtable").config.element =$(element);
    $$("detailtable").config.vendorid = id;
    $$("detailtable").clearAll();
    $$("detailtable").parse(vendor.users);
    win.show();



}
