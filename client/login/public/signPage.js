webix.ready(function() {
	
		//var data = getVendor();
		webix.protoUI({
			name:"activeList"
		},webix.ui.unitlist,webix.ActiveContent);

	var unitlist = {
		view:"activeList", 
		id:"unitlist",
        select:false,
		scroll:false,		
        scheme:{
	        $sort:{
		        by:"title",
		        dir: 'asc'
	        }
        },

		uniteBy:function(obj){
			if(obj.vendorName != '' && obj.vendorName != undefined){
				return obj.vendorName.substr(0,1);
			}
			
		},
		activeContent:{
			markCheckbox:{
                    view:"checkbox",
					labelRight:"未签到",
                    width:150,
                    on:{ /*checkbox onChange handler*/
                        'onChange':function(newv, oldv){

                            var item_id = this.config.$masterId;
							var textVal = this.config.labelRight;
							if(textVal === '已签到'){
								this.setValue(true);
								return;
							}else{
								this.config.labelRight = '已签到';
							   var state = this.getValue()?"Check ":"Uncheck ";
							}
                           
                            //webix.message(state+item_id);
                        }
                    }
                }
		},
		template:"<div class='checkbox'>{common.markCheckbox()}</div>"+"<div class='title'>#vendorName#</div>",
		type: {
				height:35
			},
			url: "/api/vendor", 
			//filterfields: "vendorName,vendorAddress,technique",
				//data:webix.copy(small_wide_film_set),
		pager:"bPage"
	};

	webix.ui({ container:"list", rows:[
		{
                height: 35,
                view:"toolbar",
                elements:[
                    {view:"text", id:"list_input",css:"fltr", labelWidth:170},
					{
			view:"pager", id:"bPage", size:10, group:5
		}
                ]
            },
		unitlist
		
	]});
 $$("list_input").attachEvent("onTimedKeyPress",function(){
        var value = this.getValue().toLowerCase();
        $$("unitlist").filter(function(obj){
			if(obj.vendorName != '' && obj.vendorName != undefined){
				 return obj.vendorName.toLowerCase().indexOf(value)==0;
			}
           
        })
    });
});