﻿import crypto = require("crypto");
import _ = require("lodash");

//#region加解密
export function MD5(data) {
    var _encrymd5 = crypto.createHash('md5');
    _encrymd5.update(data);
    return _encrymd5.digest('hex');
}
var cipheriv = function (en, code, data) {
    var buf1 = en.update(data, code), buf2 = en.final();
    var r = new Buffer(buf1.length + buf2.length);
    buf1.copy(r); buf2.copy(r, buf1.length);
    return r;
};
export function EncryptDES (data, key, vi) {
    return data = cipheriv(crypto.createCipheriv('des', key, vi), 'utf8', data).toString('base64');
};
export function DecryptDES(data, key, vi) {
    return cipheriv(crypto.createDecipheriv('des', key, vi), 'base64', data).toString('utf8');
};
//#endregion

//#region 树节点处理
function _arrayToHash(dataArray, keyField) {
    var handler = null;
    if (typeof keyField == "function")
        handler = keyField;

    var hash = {};
    for (var i = 0; i < dataArray.length; i++) {
        var data = dataArray[i];

        if (handler) {
            handler(data);
            continue;
        }

        var key = data[keyField];
        hash[key] = data;
    }

    return hash;
}
export function Tree(data, fields?) {
    fields = fields || {};

    this._field_id = fields.id || "id";
    this._field_parent_id = fields.parent_id || "parent";
    this._field_children = fields.field_children || "data";

    this._elements = data;

    var dataTree = _dataToTree.call(this,data);
    return dataTree;
}

function _dataToTree(data) {
    data = _.clone(data);

    var dataHash = _arrayToHash(data, this._field_id),
        fieldParentId = this._field_parent_id,
        fieldChildren = this._field_children,
        rootElements = [];

    for (var key in dataHash) {
        var node = dataHash[key],
            parentId = node[fieldParentId];

        if (!dataHash.hasOwnProperty(parentId)) {
            node.open = true;
            rootElements.push(node);
            continue;
        }

        var parent = dataHash[parentId];
        parent[fieldChildren] = parent[fieldChildren] || [];
        parent[fieldChildren].push(node);
    }
    return rootElements;
};
//#endregion

export function WebixError(err, req, res, next) {
    console.log(err);
    if (err && req.url.indexOf('/api') === 0) {
        if (req.method === "GET") {
            res.send(500, []);
        } else {
            res.send(500, { status: "error" })
        }
    } else
    {
        next();
    }
    
}