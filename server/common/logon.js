"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const express = require("express");
const util = require("./util");
const db = require('./db');
var users = db.collection("users");
var router = express.Router();
router.post("/logon", (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    var logonForm = req.body;
    if (!logonForm) {
        res.redirect("/logon.html");
    }
    else {
        var pass = util.MD5(logonForm.password);
        var user = yield users.findOneAsync({ userName: logonForm.username, passWord: pass, status: "1" });
        if (user) {
            user.id = user._id;
            delete user.passWord;
            delete user._id;
            req.session["user"] = user;
            res.redirect("/");
        }
        else {
            res.redirect("/logon.html");
        }
    }
}));
router.get("/logout", (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    req.session.destroy(() => {
        res.redirect("/logon.html");
    });
}));
router.get("/sign", (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    req.session.destroy(() => {
        res.redirect("./metroSignPage.html");
    });
}));
router.post("/changePassword", (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    var user = req.session["user"];
    if (user) {
        var logonForm = req.body;
        var pass = util.MD5(logonForm.OldPassword);
        var dbuser = yield users.findOneAsync({ userName: user.userName, passWord: pass });
        if (dbuser) {
            var newpass = util.MD5(logonForm.NewPassword);
            var err = yield users.updateByIdAsync(dbuser._id, { $set: { passWord: newpass } });
            res.send({ result: "OK" });
        }
        else {
            res.send({ result: "Error" });
        }
    }
    else {
        res.send({ result: "Error" });
    }
}));
router.use((req, res, next) => __awaiter(this, void 0, void 0, function* () {
    if (req.session["user"] || req.url === "/metroSignPage.html" || req.url === "/supplier_sign.html" || req.url === "/api/vendor" || req.url.indexOf('/api/vendor/') >= 0) {
        next();
    }
    else {
        // var user = await users.findOneAsync({ userName: "admin", status: "1" });
        // if (user) {
        //     user.id = user._id;
        //     delete user.passWord;
        //     delete user._id;
        //     req.session["user"] = user;
        //     next();
        // } else {
        //     res.redirect("/logon.html");
        // }
        res.redirect("/logon.html");
    }
}));
module.exports = router;
//# sourceMappingURL=logon.js.map