var sha1 = require('sha1');
var WechatAPI = require('wechat-api');
import * as fs from "fs";

class GWechat{
    /**
     * 参数
     */
    token:string;
    appid:string;
    appSecret:string;
    openid:string;
    wxapi:any;
    /**
     * 构造函数
     */
    constructor(wxtoken:string,wxappid:string,wxappSecret:string){
        this.appid = wxappid;
        this.appSecret = wxappSecret;
        this.token = wxtoken;
        this.wxapi = new WechatAPI(wxappid,wxappSecret);
    };
    /**
     * 校验token
     */
    validteToken(signature:string,timestamp:string,nonceStr:string,echoStr:string){
    // 按照字典排序
	    var array = [this.token, timestamp, nonceStr];
	    array.sort();
        
        var original = sha1(array.join(""));
        if (signature === original) {
            console.log("Confirm and send echo back:"+echoStr);
            return echoStr;
        }else{

            return 'false';
        }
    };
    /**
     * 校验token
     */
    chechSignature(req:any){
        var signature = req.query.signature;
        var timestamp = req.query.timestamp;
        var nonceStr = req.query.nonce;
        var echoStr = req.query.echostr;

        // 按照字典排序
	    var array = [this.token, timestamp, nonceStr];
	    array.sort();

         var original = sha1(array.join(""));
         if (signature === original) {
            console.log("Confirm and send echo back:"+echoStr);
            return true;
        }else{
            return false;
        }
    };
    /**
     * 初始化菜单
     */
    initMenu(menus:any){
        if (!this.wxapi) {
            this.wxapi = new WechatAPI(this.appid,this.appSecret);
        }
        if (menus==undefined) {
            var wxconfig =  JSON.parse(fs.readFileSync("./config.json", "utf8"));
            menus = wxconfig.wx.wx_menu;
        }
        this.wxapi.createMenu(menus,function(err,result){
            if(err){
                console.log(err);
            }else{
                console.log(result);
            }
        });

    };
    getJsConfig(debug:boolean,jsAPILst:any,url:string){
        var param = {
            debug:debug,
            jsApiList:jsAPILst,
            url:url
        };
        this.wxapi.getJsConfig(param,function(err,result){
            if(err){
                console.log(err);
                return null;
            }else{
                return result;
            }
        })
    };


    
}

var wxconfig =  JSON.parse(fs.readFileSync("./config.json", "utf8"));
var app_id      = wxconfig.wx.appID;
var app_secret  = wxconfig.wx.appsecret;
var app_token   = wxconfig.wx.token;

export = module.exports = new GWechat(app_token,app_id,app_secret);