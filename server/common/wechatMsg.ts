class WechatMessage {
    textMsg(msg) {
        var time = Math.round(new Date().getTime() / 1000);
	
	    var funcFlag = msg.funcFlag ? msg.funcFlag : 0;
        var output = "" +
            "<xml>" +
            "<ToUserName><![CDATA[" + msg.toUserName + "]]></ToUserName>" +
            "<FromUserName><![CDATA[" + msg.fromUserName + "]]></FromUserName>" +
            "<CreateTime>" + time + "</CreateTime>" +
            "<MsgType><![CDATA[" + msg.msgType + "]]></MsgType>" +
            "<Content><![CDATA[" + msg.content + "]]></Content>" +
            "<FuncFlag>" + funcFlag + "</FuncFlag>" +
            "</xml>";
            return output;
    };
};