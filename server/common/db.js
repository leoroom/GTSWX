"use strict";
var Promise = require("bluebird");
var mongoskin = require("mongoskin");
Object.keys(mongoskin).forEach(function (key) {
    var value = mongoskin[key];
    if (typeof value === "function") {
        Promise.promisifyAll(value);
        Promise.promisifyAll(value.prototype);
    }
});
Promise.promisifyAll(mongoskin);
var dburl = process.env.DBURL || "mongodb://127.0.0.1/GTS";
var db = mongoskin.db(dburl, { native_parser: true });
module.exports = db;
//# sourceMappingURL=db.js.map