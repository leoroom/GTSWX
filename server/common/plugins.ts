import * as db from "./db";
import * as _ from "lodash";
import * as express from "express";

export function webix_pager_param(req: express.Request, res: express.Response, next: Function) {
    req.query.start = parseInt(req.query.start || 0);
    req.query.count = parseInt(req.query.count || 50);
    if (next) next();
}

export function webix_sort_param(req: express.Request, res: express.Response, next: Function) {
    var sort = req.query.sort || {};
    for (var key in sort) {
        if (sort.hasOwnProperty(key)) {
            var element = sort[key];
            sort[key] = element == "asc" ? 1 : -1;
        }
    }
    req.query.sort=sort;
    if (next) next();
}
export function webix_filter_param(req: express.Request, res: express.Response, next: Function) {
    var filter = req.query.filter || {};
    var or = req.query.or == "true";

    for (var key in filter) {
        if (filter.hasOwnProperty(key)) {
            var element = filter[key];
            if (element == "") delete filter[key];
            else filter[key] = new RegExp(element, "i");
        }
    }

    if (or) {
        var orfilter = [];
        _.forIn(filter, function (value, key) {
            var oritem = {};
            oritem[key] = value;
            orfilter.push(oritem);
        })
        filter= { $or: orfilter };
    }
    
    if(req.query.and)
    {
        filter={$and:[req.query.and,filter]}
    }

     req.query.filter = filter;
    if (next) next();
}

export function webix_all(tablename: string,needsend?:any,select?:any) {
    return async function name(req: express.Request, res: express.Response, next: Function) {
        if(needsend===undefined)
            needsend=true;
        select=select||{};

        webix_pager_param(req, res, null);
        webix_sort_param(req, res, null);
        webix_filter_param(req, res, null);

        var items = db.collection(tablename);
        var allcount = await items.countAsync(req.query.filter);
        if (req.query.count == -1) {
            res.send({ data: [], total_count: allcount, pos: req.query.start });
            return;
        }
      
        var data = await items.find(req.query.filter,select).sort(req.query.sort).skip(req.query.start).limit(req.query.count).toArrayAsync();
        
         _.each(data, row => {
            row.id = row._id;
            delete row._id;
        });
        
        req["senddata"]={data:data,total_count:allcount,pos:req.query.start};
        if(needsend) {
             res.send(req["senddata"]);
        }
        else {
           if(next) next();
        }
    }
}