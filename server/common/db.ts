﻿var Promise = require("bluebird");
var mongoskin = require("mongoskin");
Object.keys(mongoskin).forEach(function (key) {
    var value = mongoskin[key];
    if (typeof value === "function") {
        Promise.promisifyAll(value);
        Promise.promisifyAll(value.prototype);
    }
});
Promise.promisifyAll(mongoskin);
var dburl=process.env.DBURL||"mongodb://127.0.0.1/GTS";
var db = mongoskin.db(dburl, { native_parser: true });

//var addr = process.env.DBSERVER_PORT_27017_TCP_ADDR;
//var port = process.env.DBSERVER_PORT_27017_TCP_PORT;
//var dburl="mongodb://gts:GTSGROUP-2015@"+ addr +":"+port+"/GTS"||"mongodb://127.0.0.1/GTS";

//var db = mongoskin.db("mongodb://gtsdb-jianlv.myalauda.cn:10188/GTS", { native_parser: true });
export = db;