﻿import * as express from "express";
import * as session from "express-session";
import * as path from "path";
import util = require("./util");
import db = require('./db');
var users = db.collection("users");
var router = express.Router();

router.post("/logon", async (req: express.Request, res: express.Response, next) => {
    var logonForm = req.body;
    if (!logonForm) {
        res.redirect("/logon.html");
    } else {
        var pass = util.MD5(logonForm.password);
        var user = await users.findOneAsync({ userName: logonForm.username, passWord: pass, status: "1" });
        if (user) {
            user.id = user._id;
            delete user.passWord;
            delete user._id;
            req.session["user"] = user;
            res.redirect("/");
        } else {
            res.redirect("/logon.html");
        }
    }
});

router.get("/logout", async (req: express.Request, res: express.Response, next) => {
    req.session.destroy(() => {
        res.redirect("/logon.html");
    });
});
router.get("/sign", async (req: express.Request, res: express.Response, next) => {
    req.session.destroy(() => {
        res.redirect("./metroSignPage.html");
    });
});
router.post("/changePassword", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    if (user) {
        var logonForm = req.body;
        var pass = util.MD5(logonForm.OldPassword);
        var dbuser = await users.findOneAsync({ userName: user.userName, passWord: pass });
        if (dbuser) {
            var newpass = util.MD5(logonForm.NewPassword);
            var err = await users.updateByIdAsync(dbuser._id, { $set: { passWord: newpass } });
            res.send({ result: "OK" });
        } else {
            res.send({ result: "Error" });
        }
    } else {
        res.send({ result: "Error" });
    }
});
router.use(async (req: express.Request, res: express.Response, next) => {
    if (req.session["user"] || req.url === "/metroSignPage.html"|| req.url === "/supplier_sign.html"||req.url === "/api/vendor" || req.url.indexOf('/api/vendor/')>=0) {
        next();
    } else {
        // var user = await users.findOneAsync({ userName: "admin", status: "1" });
        // if (user) {
        //     user.id = user._id;
        //     delete user.passWord;
        //     delete user._id;
        //     req.session["user"] = user;
        //     next();
        // } else {
        //     res.redirect("/logon.html");
        // }

        res.redirect("/logon.html");
    }
});

export =router;