﻿import * as express from "express";
import * as db from "../../common/db";
import * as _ from "lodash";
import * as moment from "moment";
var xlsx = require("node-xlsx");
var formidable = require("formidable");

var router = express.Router();

var RootPath = process.env.RootPath;
var TempPath = process.env.TempPath;
var PicPath = process.env.PicPath;
var DocPath = process.env.DocPath;

//#region 增删改查

router.get("/:table", async (req: express.Request, res: express.Response, next) => {
    var table = req.param("table");
    var items = db.collection(table);
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.post("/:table", async (req: express.Request, res: express.Response, next) => {
    var table = req.param("table");
    var items = db.collection(table);
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:table/:id", async (req: express.Request, res: express.Response, next) => {
    var table = req.param("table");
    var items = db.collection(table);
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:table/:id", async (req: express.Request, res: express.Response, next) => {
    var table = req.param("table");
    var items = db.collection(table);
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

router.post("/:table/upload/:index", async (req: express.Request, res: express.Response, next) => {
    var form = new formidable.IncomingForm();
    form.uploadDir = RootPath + TempPath;
    form.encoding = 'utf-8';
    form.keepExtensions = true;
    var filename;
    form.on('file', function (name, file) {
        filename=name;
        var table = req.param("table");
        var indexstr = req.param("index") || "0";
        var indexs = indexstr.split(",").map(str => { return parseInt(str) });
        var items = db.collection(table);
        var sheets = xlsx.parse(file.path);
        var sheet = (sheets[0] || {}).data;

        var bulk = items.initializeUnorderedBulkOp();
        var head = sheet[0];
        for (var i = 1; i < sheet.length; i++) {
            var row = {};
            var searchkeys = {};
            for (var j = 1; j < head.length; j++) {
                var title = head[j];
                row[title] = sheet[i][j];
                if (indexs.indexOf(j) >= 0) {
                    searchkeys[title] = sheet[i][j];
                }
            }
            bulk.update(searchkeys, row, { upsert: true });
        };
    });
    form.on('end', function () {
        res.send({ status: "server", sname: filename });
    });
    form.on('error', function (err) {
        console.log("error:" + err);
        res.send({ status: "error" });
    });
    form.parse(req);
});
export = router;
