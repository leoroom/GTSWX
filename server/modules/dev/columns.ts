﻿import * as express from "express";
import * as db from "../../common/db";
import * as _ from "lodash";
import * as moment from "moment";

var ObjectID = require("mongodb").ObjectID;
var router = express.Router();
var items = db.collection("sys.columns");
//#region 增删改查

router.get("/:table", async (req: express.Request, res: express.Response, next)=> {
    var table = req.param("table");
    var tableinfo = await items.findOneAsync({ table: table });
    tableinfo = tableinfo || {};
    res.send(tableinfo.columns);
});
router.post("/:table", async (req: express.Request, res: express.Response, next)=> {
    var table = req.param("table");
    var obj = req.body;
    obj.id = new ObjectID().toHexString();
    obj.seq = parseInt(obj.seq);
    obj.width = parseInt(obj.width);
    obj.fillspace = obj.fillspace=="true";
    obj.display = obj.display == "true";
    obj.created = new Date();
    delete obj.ch;
    var record = await items.updateOneAsync({ table: table }, { $push: { columns: obj } }, {upsert:true});
    res.send({ newid: obj.id });
});
router.put("/:table/:id", async (req: express.Request, res: express.Response, next)=> {
    var table = req.param("table");
    var id = req.param("id"); 
    var obj = req.body;
    if (id != obj.id) { return; }
    obj.seq = parseInt(obj.seq);
    obj.width = parseInt(obj.width);
    obj.fillspace = obj.fillspace == "true";
    obj.display = obj.display == "true";
    obj.updated = new Date();
    delete obj.ch;
    var record = await items.updateOneAsync({ table: table,"columns.id":id }, { $set: { "columns.$": obj } });
    res.send({});
});
router.delete("/:table/:id", async (req: express.Request, res: express.Response, next)=> {
    var table = req.param("table");
    var items = db.collection(table);
    var id = req.param("id");
    var record = await items.updateOneAsync({ table: table }, { $pull: { columns: {id:id} } });
    res.send({});
});

router.get("/:table/show", async (req: express.Request, res: express.Response, next)=> {
    var table = req.param("table");
    var tableinfo = await items.findOneAsync({ table: table });
    if (!tableinfo) { res.send([]); return; }

    var showcolumns = _.chain(tableinfo.columns).orderBy(["seq"], ["asc"]).filter({ display: true }).map((row: any) => {
        row.id = row.field;
        delete row.field;
        delete row.seq;
        delete row.display;
        delete row.updated;
        delete row.created;
        if (row.fillspace) {
            delete row.width;
        } else {
            delete row.fillspace;
        }
        if (row.footer == "") {
            delete row.footer;
        }
        return row;
    }).value();

    showcolumns.push({ id: "operation", header: "Operation", width: 100, template: "<span style=''color:#777777;cursor:pointer;' class='webix_icon fa-pencil gts_edit'></span> <span style='color:#777777;cursor:pointer;' class='webix_icon fa-trash-o gts_delete'></span>" });
    res.send(showcolumns);
});

export= router;
