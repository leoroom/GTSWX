import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
import util = require("../../common/util");
import * as plugins from "../../common/plugins"
var router = express.Router();

router.get("/equipment/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.vendorCode = user.vendorCode;
    req.query.and = filter;
    next();
}, plugins.webix_all("equipments"));

router.get("/equipment/select", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var items = db.collection("equipments");
    var datas = await items.find({ vendorCode: user.vendorCode }).toArrayAsync();
    var rs = datas.map(row => {
        return {
            id: row.equipmentCode,
            value: row.equipmentName,
            equipmentType: row.equipmentType
        };
    })
    res.send(rs);
});
router.post("/equipment/", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    obj.created = new Date();
    var user = req.session["user"];
    obj.vendorCode = user.vendorCode;
    obj.vendorName = user.vendorName;
    delete obj.ch;

    var items = db.collection("equipments");
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/equipment/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    var user = req.session["user"];
    obj.vendorCode = user.vendorCode;
    obj.vendorName = user.vendorName;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;

    var items = db.collection("equipments");
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/equipment/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");

    var items = db.collection("equipments");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

router.get("/mould/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.vendorCode = user.vendorCode;
    req.query.and = filter;
    next();
}, plugins.webix_all("moulds"));

router.get("/mould/select", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var items = db.collection("moulds");
    var datas = await items.find({ vendorCode: user.vendorCode }).toArrayAsync();
    var rs = datas.map(row => {
        return {
            id: row.mouldCode,
            value: row.mouldName,
            equipmentType: row.equipmentType
        };
    })
    res.send(rs);
});
router.post("/mould/", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    obj.created = new Date();
    var user = req.session["user"];
    obj.vendorCode = user.vendorCode;
    obj.vendorName = user.vendorName;
    delete obj.ch;

    var items = db.collection("moulds");
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/mould/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    var user = req.session["user"];
    obj.vendorCode = user.vendorCode;
    obj.vendorName = user.vendorName;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;

    var items = db.collection("moulds");
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/mould/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");

    var items = db.collection("moulds");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

router.get("/:vendorCode/equipment/select", async (req: express.Request, res: express.Response, next) => {
    var vendorCode = req.params.vendorCode;
    var user = req.session["user"];
    var items = db.collection("equipments");
    var vendor = await db.collection("vendors").findOneAsync({ vendorCode: user.vendorCode });
    var index = _.findIndex(vendor.suppliers, { supplierCode: vendorCode });
    if (index >= 0) {
        var datas = await items.find({ vendorCode: vendorCode }).toArrayAsync();
        var rs = datas.map(row => {
            return {
                id: row.equipmentCode,
                value: row.equipmentName,
                equipmentType: row.equipmentType
            };
        })
        res.send(rs);
    } else {
        res.send([]);
    }

});

router.get("/:vendorCode/mould/select", async (req: express.Request, res: express.Response, next) => {
    var vendorCode = req.params.vendorCode;
    var user = req.session["user"];
    var items = db.collection("moulds");
    var vendor = await db.collection("vendors").findOneAsync({ vendorCode: user.vendorCode });
    var index = _.findIndex(vendor.suppliers, { supplierCode: vendorCode });
    if (index >= 0) {
        var datas = await items.find({ vendorCode: user.vendorCode }).toArrayAsync();
        var rs = datas.map(row => {
            return {
                id: row.mouldCode,
                value: row.mouldName,
                equipmentType: row.equipmentType
            };
        })
        res.send(rs);
    } else {
        res.send([]);
    }

});

export = router;
