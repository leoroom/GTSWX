import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
//#region 增删改查
var items = db.collection("products");

router.get("/", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/pager", plugins.webix_all("products"));
router.post("/", async (req: express.Request, res: express.Response, next)=> {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    if(obj.productnumber)
        obj.productnumber=JSON.parse(obj.productnumber);
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    if(obj.productnumber)
        obj.productnumber=JSON.parse(obj.productnumber);
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});
router.get("/select", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    var datas= datas.map(row=>{
        return {id:row.productName,value:row.productName,"module":row["module"]};
    })
    res.send(datas);
});
router.get("/number/select", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    var numbers=[];
    var datas= datas.forEach(row=>{
        if(row.productnumber)
           row.productnumber.forEach(subrow=>{
                numbers.push({id:subrow.id,value:subrow.productnumber,parent:row.productName});
            });
    })
    res.send(numbers);
});
//#endregion

export= router;
