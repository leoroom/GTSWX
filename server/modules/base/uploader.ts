import * as express from "express";
var qiniu = require("../../common/qiniu");
var router = express.Router();

router.get("/uptoken", (req, res) => {
    var token = qiniu.token();
    if (token) {
        res.json({
            uptoken: token
        });
    }
})

router.delete("/removefile/:key", (req, res) => {
    var key =req.params.key;
    qiniu.remove(key);
    res.send({});
})

export =router;