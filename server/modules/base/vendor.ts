﻿import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
//#region 增删改查
var items = db.collection("vendors");

router.get("/", async (req: express.Request, res: express.Response, next) => {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});

router.get("/my", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var vendor = await items.findOneAsync({ vendorCode: user.vendorCode });
    if (vendor) {
        vendor.id = vendor._id;
        delete vendor._id;
        res.send(vendor);
    } else {
        res.send({});
    }

});

router.get("/pager", plugins.webix_all("vendors"));
router.post("/", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    if (obj.contacts)
        obj.contacts = JSON.parse(obj.contacts);
    if (obj.customers)
        obj.customers = JSON.parse(obj.customers);
    if (obj.suppliers)
        obj.suppliers = JSON.parse(obj.suppliers);
    if (obj.materiels)
        obj.materiels = JSON.parse(obj.materiels);
    if (obj.warehouses)
        obj.warehouses = JSON.parse(obj.warehouses);
    if (obj.products)
        obj.products = JSON.parse(obj.products);
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    if (obj.contacts)
        obj.contacts = JSON.parse(obj.contacts);
    if (obj.customers)
        obj.customers = JSON.parse(obj.customers);
    if (obj.suppliers)
        obj.suppliers = JSON.parse(obj.suppliers);
    if (obj.materiels)
        obj.materiels = JSON.parse(obj.materiels);
    if (obj.warehouses)
        obj.warehouses = JSON.parse(obj.warehouses);
    if (obj.products)
        obj.products = JSON.parse(obj.products);
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#endregion

router.get("/richselect", async (req: express.Request, res: express.Response, next) => {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row.vendorCode;
        delete row.vendorCode;

        row.value = row.vendorName;
        delete row.vendorName;
    });
    datas.push({ id: "000000", value: "系统", vendorCode: "000000" });
    res.send(datas);
});

router.post("/:id/addcontact", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var record = await items.updateByIdAsync(id, { $push: { contacts: obj } })
    res.send({ newid: id });
});

router.get("/my/product/richselect", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var vendor = await items.findOneAsync({ vendorCode: user.vendorCode });
    _.each(vendor.products, row => {
        row.id = row.productName;
        row.value = row.productName;
        delete row.productName;
        _.each(row.productNumber, n => {
            n.id = n.productNumber;
            n.value = n.productNumber;
            delete n.productNumber;
        });
    });
    res.send(vendor.products);
});
router.get("/my/materials/richselect", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var vendor = await items.findOneAsync({ vendorCode: user.vendorCode });
    _.each(vendor.materiels, row => {
        row.id = row.materielCode;
        row.value = row.materielName;
        delete row.materielCode;
        delete row.productName;
    });
    res.send(vendor.materiels);
});
router.get("/:vendorCode/product/richselect", async (req: express.Request, res: express.Response, next) => {

    var vendorCode = req.params.vendorCode;
    var user = req.session["user"];
    var vendor = await items.findOneAsync({ vendorCode: user.vendorCode });
    var index = _.findIndex(vendor.suppliers, { supplierCode: vendorCode });
    if (index >= 0) {
        var supplier = await items.findOneAsync({ vendorCode: vendorCode });
        _.each(supplier.products, row => {
            row.id = row.productName;
            row.value = row.productName;
            delete row.productName;
            _.each(row.productNumber, n => {
                n.id = n.productNumber;
                n.value = n.productNumber;
                delete n.productNumber;
            });
        });
        res.send(supplier.products);
    } else {
        res.send([]);
    }


});
router.get("/:vendorCode/materials/richselect", async (req: express.Request, res: express.Response, next) => {
    var vendorCode = req.params.vendorCode;
    var user = req.session["user"];
    var vendor = await items.findOneAsync({ vendorCode: user.vendorCode });
    var index = _.findIndex(vendor.suppliers, { supplierCode: vendorCode });
    if (index >= 0) {
        var supplier = await items.findOneAsync({ vendorCode: vendorCode });
        _.each(supplier.materiels, row => {
            row.id = row.materielCode;
            row.value = row.materielName;
            delete row.materielCode;
            delete row.productName;
        });
        res.send(supplier.materiels);
    } else {
        res.send([]);
    }

});
export = router;
