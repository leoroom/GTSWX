import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
//#region 增删改查
var items = db.collection("price");

router.get("/:billing", async (req: express.Request, res: express.Response, next)=> {
    var billing=req.param("billing");
    var datas = await items.find({billing:billing}).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/service/:type", async (req: express.Request, res: express.Response, next)=> {
    var type=req.param("type");
    var datas = await items.find({serviceType:type}).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/:billing/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var billing=req.param("billing");
    filter.billing = billing;
    req.query.and = filter;
    next();
}, plugins.webix_all("price"));

router.post("/", async (req: express.Request, res: express.Response, next)=> {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    // if (obj.contacts)
    //     obj.contacts = JSON.parse(obj.contacts);
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    // if (obj.contacts)
    //     obj.contacts = JSON.parse(obj.contacts);
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#endregion

export= router;
