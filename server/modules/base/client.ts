﻿import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
//#region 增删改查
var items = db.collection("clients");

router.get("/", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/pager", plugins.webix_all("clients"));
router.post("/", async (req: express.Request, res: express.Response, next)=> {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    if (obj.contacts)
        obj.contacts = JSON.parse(obj.contacts);
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    if (obj.contacts)
        obj.contacts = JSON.parse(obj.contacts);
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#endregion

router.get("/richselect", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;

        row.value = row.clientName;
        delete row.clientName;
    });
    res.send(datas);
});

router.post("/:id/addcontact", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var record = await items.updateByIdAsync(id, { $push: { contacts: obj}})
    res.send({ newid: id });
});
export= router;
