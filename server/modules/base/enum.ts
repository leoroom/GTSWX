import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
//#region 增删改查
var items = db.collection("enums");

router.get("/", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/pager", plugins.webix_all("enums"));
router.post("/", async (req: express.Request, res: express.Response, next)=> {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#endregion

router.get("/:module/select", async (req: express.Request, res: express.Response, next)=> {
    var key=req.param("module");
    var datas = await items.find({"module":key}).toArrayAsync();
    var datas= datas.map(row=>{return {id:row.ekey,value:row.evalue}});
    res.send(datas);
});

export= router;
