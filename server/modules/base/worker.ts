import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
//#region 增删改查
var items = db.collection("workers");

router.get("/", async (req: express.Request, res: express.Response, next)=> {
   var user = req.session["user"];
    var datas = await items.find({managerId: user.id}).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.managerId = user.id;
    req.query.and = filter;
    next();
}, plugins.webix_all("workers"));
router.post("/", async (req: express.Request, res: express.Response, next)=> {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var user = req.session["user"];
    obj.managerId= user.id;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var user = req.session["user"];
    obj.managerId= user.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#endregion
router.get("/select", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    var datas= datas.map(row=>{return {id:row.name,value:row.name}});
    res.send(datas);
});
export= router;
