import * as fs from "fs";
var WechatAPI = require("wechat-api");

var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));
/**
 * 微信接口二维码管理模块
 */
export class WxQRCode {
     /**
    * 参数
    */
    token: string;
    app_id: string;
    app_secret: string;
    openid: string;
    app_encodingAESKey: string;
    params: any;
    api:any;
    constructor(args) {
        if (args) {
            this.params = args;

            if (args.openid) {

            this.openid = args.openid;

            }
        }
        
        this.app_id             = wxconfig.wx.appID;
        this.app_secret         = wxconfig.wx.appsecret;
        this.app_encodingAESKey = wxconfig.wx.encodingAESKey;
        this.token              = wxconfig.wx.token;
        this.api                = new WechatAPI(this.app_id, this.app_secret);
    };
    /**
     * 创建临时服务号二维码
     * 参数1：sceneId：number，场景ID。数字ID不能大于100000，字符串ID长度限制为1到64
     * 参数2：expire：number，过期时间，单位秒。最大不超过604800（即7天
     * result:{
     "ticket":"gQG28DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0FuWC1DNmZuVEhvMVp4NDNMRnNRAAIEesLvUQMECAcAAA==",
     "expire_seconds":1800
    }
    通过ticket票据生成二维码图片
     */
    createTmpQRCode(sceneId:any){
        var sceneId = sceneId;
        if (!sceneId) {
           return null;
        }
        var expire = 604800;//过期时间，单位秒。最大不超过604800（即7天
         this.api.createTmpQRCode(sceneId, expire, function (err, result) {
            if (err) {
                return err;
            } else {
                return result;
            }
        });
    };
    /**
     * 生成永久二维码
     * 参数1：sceneId(Number,String)场景ID。数字ID不能大于100000，字符串ID长度限制为1到64
     * result：
     * {
     "ticket":"gQG28DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0FuWC1DNmZuVEhvMVp4NDNMRnNRAAIEesLvUQMECAcAAA=="
    }
     */
    createLimitQRCode(sceneId) {
        var sceneId = sceneId;
        if (!sceneId) {
            return null;
        }
        this.api.createLimitQRCode(sceneId, function (err, result) {
            if (err) {
                return err;
            } else {
                return result;
            }
        });
    };
    /**
     * 通过票据获取二维码图片地址
     * 参数：ticket:string,调用生成二维码方法获取到ticket
     * result:String 显示二维码的URL地址，通过img标签可以显示出来
     */
    getQRCodeUrl(ticket){
        var ticket = ticket;
        if (!ticket) {
            return null;
        }

        var qrCodeUrl = this.api.showQRCodeURL(ticket);
        return qrCodeUrl;
    };
};

//export = WxQRCode;