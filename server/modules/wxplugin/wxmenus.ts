var WechatAPI = require("wechat-api");
import * as fs from "fs";

var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));
/**
 * 菜单管理模块
 */
export class WxMenus {
    /**
    * 参数
    */
    token: string;
    app_id: string;
    app_secret: string;
    openid: string;
    app_encodingAESKey: string;
    params: any;
    api:any;
    constructor(args) {
        if (args) {
            this.params = args;

            if (args.openid) {

            this.openid = args.openid;

            }
        }
        
        this.app_id             = wxconfig.wx.appID;
        this.app_secret         = wxconfig.wx.appsecret;
        this.app_encodingAESKey = wxconfig.wx.encodingAESKey;
        this.token              = wxconfig.wx.token;
        var menu_config         = wxconfig.wx.wx_menu;
        this.api                = new WechatAPI(this.app_id, this.app_secret);
    };
    /**
     * 创建自定义菜单
     * menus:菜单对象
     * {
      "button":[
        {
          "type":"click",
          "name":"今日歌曲",
          "key":"V1001_TODAY_MUSIC"
        },
        {
          "name":"菜单",
          "sub_button":[
            {
              "type":"view",
              "name":"搜索",
              "url":"http://www.soso.com/"
            },
            {
              "type":"click",
              "name":"赞一下我们",
              "key":"V1001_GOOD"
            }]
          }]
        }
      ]
     }
     * callbackFun:回调函数
     * result:
     * {"errcode":0,"errmsg":"ok"}
     */
    createMenu(menus, callbackFun) { 
        if(typeof(callbackFun) == 'function'){
            this.api.createMenu(menus, callbackFun);
        }else{
            this.api.createMenu(menus, function (err,result) {
                return err?err:result;
            });
        }
    };
    /***
     * 获取菜单
     * result:
     * {
      "menu": {
        "button":[
          {"type":"click","name":"今日歌曲","key":"V1001_TODAY_MUSIC","sub_button":[]},
          {"type":"click","name":"歌手简介","key":"V1001_TODAY_SINGER","sub_button":[]},
          {"name":"菜单","sub_button":[
          {"type":"view","name":"搜索","url":"http://www.soso.com/","sub_button":[]},
          {"type":"view","name":"视频","url":"http://v.qq.com/","sub_button":[]},
          {"type":"click","name":"赞一下我们","key":"V1001_GOOD","sub_button":[]}]
          }
          ]
          }
          }
     */
    getMenu(callbackFun) { 
        if(typeof(callbackFun) == 'function'){
            this.api.getMenu(callbackFun);
        }else{
            this.api.getMenu(function (err,result) {
                return err?err:result;
            });
        }
    };
    /***
     * 删除自定义菜单
     * result:
     * 
     */
    removeMenu(callbackFun) { 
        if(typeof(callbackFun) == 'function'){
            this.api.removeMenu(callbackFun);
        }else{
            this.api.removeMenu(function (err,result) {
                return err?err:result;
            });
        }
    };
    /***
     * 获取自定义菜单配置
     * result:
     * 
     */
    getMenuConfig(callbackFun) { 
        if(typeof(callbackFun) == 'function'){
            this.api.getMenuConfig(callbackFun);
        }else{
            this.api.getMenuConfig(function (err,result) {
                return err?err:result;
            });
        }
    };
};

//export = WxMenus;