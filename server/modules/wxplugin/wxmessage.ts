var WechatAPI = require("wechat-api");
import * as fs from "fs";

var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));

/**
 * 微信接口消息推送管理模块
 */
export class WxMessage {
    /**
    * 参数
    */
    token: string;
    app_id: string;
    app_secret: string;
    openid: string;
    app_encodingAESKey: string;
    params: any;
    api:any;
    constructor(args) {
        if (args) {
            this.params = args;

            if (args.openid) {

            this.openid = args.openid;

            }
        }
        
        this.app_id             = wxconfig.wx.appID;
        this.app_secret         = wxconfig.wx.appsecret;
        this.app_encodingAESKey = wxconfig.wx.encodingAESKey;
        this.token              = wxconfig.wx.token;
        this.api                = new WechatAPI(this.app_id, this.app_secret);
    };
    /**
     * 客服消息，发送文字消息
     * 参数：
     * openid，发送对象openid
     * text，发送文字内容
     */
    sendText(openid:string,text:string,callBackFun:any){
        if (typeof (callBackFun) == 'function') {
            this.api.sendText(openid, text, callBackFun);
        } else {
            this.api.sendText(openid, text, function (err, result) {
                return err?err:result;
            });
        }
        
    };
    /**
     * 客服消息，发送图片消息
     * 参数：
     * openid，发送对象openid
     * media_id,媒体文件的ID，参见uploadMedia方法
     */
    sendImage(openid: string, media_id: string, callBackFun: any) {
        if (typeof (callBackFun) == 'function') {
            this.api.sendImage(openid, media_id, callBackFun);
        } else {
            this.api.sendImage(openid, media_id, function (err, result) {
                if (err) {
                    return err;
                } else {
                    return result;
                }
            });
        }
    };
    /**
     * 客服消息，发送语音消息
     * 参数：
     * openid，发送对象openid
     * media_id,媒体文件的ID
     */
    sendVoice(openid: string, media_id: string, callBackFun: any) {
        if (typeof (callBackFun) == 'function') {
            this.api.sendVoice(openid, media_id, callBackFun);
        } else {
            this.api.sendVoice(openid, media_id, function (err, result) {
                if (err) {
                    return err;
                } else {
                    return result;
                }
            });
        }
    };
    /**
     * 客服消息，发送视频消息
     * 参数：
     * openid，发送对象openid
     * media_id,媒体文件的ID
     * thumbMediaId,缩略图文件的ID
     */
    sendVideo(openid: string, media_id: string,thumbMediaId:string, callBackFun: any) {
        if (typeof (callBackFun) == 'function') {
            this.api.sendVideo(openid, media_id,thumbMediaId, callBackFun);
        } else {
            this.api.sendVideo(openid, media_id,thumbMediaId, function (err, result) {
                if (err) {
                    return err;
                } else {
                    return result;
                }
            });
        }
    };
    /**
     * 客服消息，发送音乐消息
     * 参数：
     * openid，发送对象openid
     * music,音乐文件
     * var music = {
         title: '音乐标题', // 可选
         description: '描述内容', // 可选
         musicurl: 'http://url.cn/xxx', 音乐文件地址
         hqmusicurl: "HQ_MUSIC_URL",
         thumb_media_id: "THUMB_MEDIA_ID"
        };
     */
    sendMusic(openid: string, music: any, callBackFun: any) {
        if (typeof (callBackFun) == 'function') {
            this.api.sendMusic(openid, music, callBackFun);
        } else {
            this.api.sendMusic(openid, music, function (err, result) {
                if (err) {
                    return err;
                } else {
                    return result;
                }
            });
        }
    };
    /**
     * 客服消息，发送图文消息（点击跳转到外链）
     * 参数：
     * openid，发送对象openid
     * articles,图文列表
     * var articles = [
     {
       "title":"Happy Day",
       "description":"Is Really A Happy Day",
       "url":"URL",
       "picurl":"PIC_URL"
     },
     {
       "title":"Happy Day",
       "description":"Is Really A Happy Day",
       "url":"URL",
       "picurl":"PIC_URL"
     }];
        };
     */
    sendNews(openid: string, articles: any, callBackFun: any) {
        if (typeof (callBackFun) == 'function') {
            this.api.sendNews(openid, articles, callBackFun);
        } else {
            this.api.sendNews(openid, articles, function (err, result) {
                if (err) {
                    return err;
                } else {
                    return result;
                }
            });
        }
    };
    /**
     * 客服消息，发送图文消息（点击跳转到图文消息页面）
     * 参数：
     * openid，发送对象openid
     * media_id,图文消息的id
     */
    sendMpNews(openid: string, mediaId: string, callBackFun: any) {
        if (typeof (callBackFun) == 'function') {
            this.api.sendMpNews(openid, mediaId, callBackFun);
        } else {
            this.api.sendMpNews(openid, mediaId, function (err, result) {
                if (err) {
                    return err;
                } else {
                    return result;
                }
            });
        }
    };
    /**
     * 客服消息，发送卡卷消息
     * 参数：
     * openid，发送对象openid
     * wxcard,卡卷相关信息
     * thumbMediaId,缩略图文件的ID
     */
    sendCard(openid: string, wxcard: any, callBackFun: any) {
        if (typeof (callBackFun) == 'function') {
            this.api.sendCard(openid, wxcard, callBackFun);
        } else {
            this.api.sendCard(openid, wxcard, function (err, result) {
                if (err) {
                    return err;
                } else {
                    return result;
                }
            });
        }
    };
    /**
     * 上传多媒体文件，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     * news:图文消息对象
     * {
      "articles": [
        {
          "thumb_media_id":"qI6_Ze_6PtV7svjolgs-rN6stStuHIjs9_DidOHaj0Q-mwvBelOXCFZiq2OsIU-p",
          "author":"xxx",
          "title":"Happy Day",
          "content_source_url":"www.qq.com",
          "content":"content",
          "digest":"digest",
          "show_cover_pic":"1"
       },
       {
          "thumb_media_id":"qI6_Ze_6PtV7svjolgs-rN6stStuHIjs9_DidOHaj0Q-mwvBelOXCFZiq2OsIU-p",
          "author":"xxx",
          "title":"Happy Day",
          "content_source_url":"www.qq.com",
          "content":"content",
          "digest":"digest",
          "show_cover_pic":"0"
       }
      ]
     }
     * callBackFun:回调函数
     * Result
     * {
     "type":"news",
      "media_id":"CsEf3ldqkAYJAU6EJeIkStVDSvffUJ54vqbThMgplD-VJXXof6ctX5fI6-aYyUiQ",
      "created_at":1391857799
     }
     */
    uploadNews(news:any, callBackFun:any){
        if(typeof(callBackFun) == 'function'){
            this.api.uploadNews(news,  callBackFun);
        }else{
            this.api.uploadNews(news,  function (err,result) {
                return err?err:result;
            });
        }
    };
    /**
     * 将通过上传下载多媒体文件得到的视频media_id变成视频素材
     * opts:待上传为素材的视频
     * callBackFun:回调函数
     * Result
     * {
      "media_id": "rF4UdIMfYK3efUfyoddYRMU50zMiRmmt_l0kszupYh_SzrcW5Gaheq05p_lHuOTQ",
      "title": "TITLE",
      "description": "Description"
      }
     */
     
    uploadMPVideo(opts:any, callBackFun:any){
        if(typeof(callBackFun) == 'function'){
            this.api.uploadMPVideo(opts,  callBackFun);
        }else{
            this.api.uploadMPVideo(opts,  function (err,result) {
                return err?err:result;
            });
        }
    };
    /**
     * 群发消息，分别有图文（news）、文本(text)、语音（voice）、图片（image）和视频（video）
     * opts:待发送的数据
     * {
         "image":{
           "media_id":"123dsdajkasd231jhksad"
         },
         "msgtype":"image"
        }
     * receivers:接收人。一个组，或者openid列表
     * callBackFun:回调函数
     */
    massSend(opts:any, receivers:any, callBackFun:any){
        if(typeof(callBackFun) == 'function'){
            this.api.massSend(opts, receivers, callBackFun);
        }else{
            this.api.massSend(opts, receivers, function (err,result) {
                return err?err:result;
            });
        }
    };
    massSendNews(){};
    /**
     * 群发文字（text）消息
     * 参数：
     * content:string ，发送文字消息内容
     * receivers:String,Array,boolean，接收人。一个组，或者openid列表
     * callbackFun,回调函数
     * Result
     * {
         "errcode":0,
         "errmsg":"send job submission success",
         "msg_id":34182
       }
     */
    massSendText(content:string, receivers:any, callbackFun:any){
        if(typeof(callbackFun) == 'function'){
            this.api.massSendText(content, receivers, callbackFun);
        }else{
            this.api.massSendText(content, receivers, function (err,result) {
                return err?err:result;
            });
        }
        
    };
    /**
     * 群发声音（voice）消息
     * 参数：
     * media_id:string ，声音media id
     * receivers:String,Array,boolean，接收人。一个组，或者openid列表
     * callbackFun,回调函数
     * Result
     * {
         "errcode":0,
         "errmsg":"send job submission success",
         "msg_id":34182
       }
     */
    massSendVoice(media_id:string, receivers:any, callbackFun:any){
         if(typeof(callbackFun) == 'function'){
            this.api.massSendVoice(media_id, receivers, callbackFun);
        }else{
            this.api.massSendVoice(media_id, receivers, function (err,result) {
                return err?err:result;
            });
        }
    };
    /**
     * 群发图片（image）消息
     * 参数：
     * media_id:string ，图片media id
     * receivers:String,Array,boolean，接收人。一个组，或者openid列表
     * callbackFun,回调函数
     * Result
     * {
         "errcode":0,
         "errmsg":"send job submission success",
         "msg_id":34182
       }
     */
    massSendImage(media_id:string, receivers:any, callbackFun:any){
            if(typeof(callbackFun) == 'function'){
            this.api.massSendVoice(media_id, receivers, callbackFun);
        }else{
            this.api.massSendVoice(media_id, receivers, function (err,result) {
                return err?err:result;
            });
        }
    };

     /**
     * 群发视频（video）消息
     * 参数：
     * media_id:string ，视频media id
     * receivers:String,Array,boolean，接收人。一个组，或者openid列表
     * callbackFun,回调函数
     * Result
     * {
         "errcode":0,
         "errmsg":"send job submission success",
         "msg_id":34182
       }
     */
    massSendVideo(media_id:string, receivers:any, callbackFun:any){
            if(typeof(callbackFun) == 'function'){
            this.api.massSendVideo(media_id, receivers, callbackFun);
        }else{
            this.api.massSendVideo(media_id, receivers, function (err,result) {
                return err?err:result;
            });
        }
    };
    
     /**
     * 群发视频（video）消息，直接通过上传文件得到的media id进行群发（自动生成素材）
     * 参数：
     * data:string ，视频数据
     * receivers:String,Array,boolean，接收人。一个组，或者openid列表
     * callbackFun,回调函数
     * Result
     * {
         "errcode":0,
         "errmsg":"send job submission success",
         "msg_id":34182
       }
     */
    massSendMPVideo(data:any, receivers:any, callbackFun:any){
            if(typeof(callbackFun) == 'function'){
            this.api.massSendMPVideo(data, receivers, callbackFun);
        }else{
            this.api.massSendMPVideo(data, receivers, function (err,result) {
                return err?err:result;
            });
        }
    };
    /**
     * 删除群发消息
     * 参数：
     * message_id:string ，待删除群发的消息id
     * receivers:String,Array,boolean，接收人。一个组，或者openid列表
     * callbackFun,回调函数
     * Result
     * {
         "errcode":0,
         "errmsg":"send job submission success",
         "msg_id":34182
       }
     */
    deleteMass(message_id:string,callbackFun:any){
        if(typeof(callbackFun) == 'function'){
            this.api.deleteMass(message_id, callbackFun);
        }else{
            this.api.deleteMass(message_id, function (err,result) {
                return err?err:result;
            });
        }
    };
     /**
     * 预览接口，预览图文消息
     * 参数：
     * openid:string ，用户openid
     * mediaId:String,图文消息mediaId
     * callbackFun,回调函数
     * Result
     * {
         "errcode":0,
         "errmsg":"send job submission success",
         "msg_id":34182
       }
     */
    previewNews(openid: string, mediaId: string, callbackFun: any) {
        if (typeof (callbackFun) == 'function') {
            this.api.previewNews(openid, mediaId, callbackFun);
        } else {
            this.api.previewNews(openid, mediaId, function (err, result) {
                return err ? err : result;
            });
        }
    };
    /**
    * 预览接口，预览文本消息
    * 参数：
    * openid:string ，用户openid
    * content:String,文本消息
    * callbackFun,回调函数
    * Result
    * {
        "errcode":0,
        "errmsg":"send job submission success",
        "msg_id":34182
      }
    */
    previewText(openid: string, content: string, callbackFun) {
        if (typeof (callbackFun) == 'function') {
            this.api.previewNews(openid, content, callbackFun);
        } else {
            this.api.previewNews(openid, content, function (err, result) {
                return err ? err : result;
            });
        }
    };
    /**
    * 预览接口，预览语音消息
    * 参数：
    * openid:string ，用户openid
    * mediaId:String,文本消息
    * callbackFun,回调函数
    * Result
    * {
        "errcode":0,
        "errmsg":"send job submission success",
        "msg_id":34182
      }
    */
    previewVoice(openid: string, mediaId: string, callbackFun){
        if (typeof (callbackFun) == 'function') {
            this.api.previewVoice(openid, mediaId, callbackFun);
        } else {
            this.api.previewVoice(openid, mediaId, function (err, result) {
                return err ? err : result;
            });
        }
    };
    /**
    * 预览接口，预览图片消息
    * 参数：
    * openid:string ，用户openid
    * mediaId:String,文本消息
    * callbackFun,回调函数
    * Result
    * {
        "errcode":0,
        "errmsg":"send job submission success",
        "msg_id":34182
      }
    */
    previewImage(openid: string, mediaId: string, callbackFun){
        if (typeof (callbackFun) == 'function') {
            this.api.previewImage(openid, mediaId, callbackFun);
        } else {
            this.api.previewImage(openid, mediaId, function (err, result) {
                return err ? err : result;
            });
        }
    };
    /**
    * 预览接口，预览视频消息
    * 参数：
    * openid:string ，用户openid
    * mediaId:String,文本消息
    * callbackFun,回调函数
    * Result
    * {
        "errcode":0,
        "errmsg":"send job submission success",
        "msg_id":34182
      }
    */
    previewVideo(openid: string, mediaId: string, callbackFun){
        if (typeof (callbackFun) == 'function') {
            this.api.previewVideo(openid, mediaId, callbackFun);
        } else {
            this.api.previewVideo(openid, mediaId, function (err, result) {
                return err ? err : result;
            });
        }
    };
    /**
    * 查询群发消息状态
    * 参数：
    * messageId:string ，消息ID
    * callbackFun,回调函数
    * Result
    * {
        "msg_id":201053012,
         "msg_status":"SEND_SUCCESS"
      }
    */
    getMassMessageStatus(messageId: string, callbackFun){
        if (typeof (callbackFun) == 'function') {
            this.api.previewVideo(messageId, callbackFun);
        } else {
            this.api.previewVideo(messageId, function (err, result) {
                return err ? err : result;
            });
        }
    };
};

//export =  WxMessage;