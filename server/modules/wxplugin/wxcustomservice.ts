var WechatAPI = require("wechat-api");
import * as fs from "fs";

var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));

/**
 * 微信客户聊天接口模块
 */
export class WxCustomService {
    /**
    * 参数
    */
    token: string;
    app_id: string;
    app_secret: string;
    openid: string;
    app_encodingAESKey: string;
    params: any;
    api:any;
    constructor(args) {
        if (args) {
            this.params = args;

            if (args.openid) {

            this.openid = args.openid;

            }
        }
        
        this.app_id             = wxconfig.wx.appID;
        this.app_secret         = wxconfig.wx.appsecret;
        this.app_encodingAESKey = wxconfig.wx.encodingAESKey;
        this.token              = wxconfig.wx.token;
        this.api                = new WechatAPI(this.app_id, this.app_secret);
    };
    /**
     * 获取客服聊天记录
     * 参数：
     * opts:object，查询条件
     * {
      "starttime" : 123456789,
      "endtime" : 987654321,
      "openid": "OPENID", // 非必须
      "pagesize" : 10,
      "pageindex" : 1,
     }
     * callbackFun:function,回调函数
     * http请求方式: POST
     * Result
     * {
      "recordlist": [
        {
          "worker": " test1",
          "openid": "oDF3iY9WMaswOPWjCIp_f3Bnpljk",
          "opercode": 2002,
          "time": 1400563710,
          "text": " 您好，客服test1为您服务。"
        },
        {
          "worker": " test1",
          "openid": "oDF3iY9WMaswOPWjCIp_f3Bnpljk",
          "opercode": 2003,
          "time": 1400563731,
          "text": " 你好，有什么事情？ "
        },
      ]
     }
     */
    getRecords(opts:any, callBackFun:any){
        if (typeof (callBackFun) == 'function') {
            this.api.getRecords(opts, callBackFun);
        } else {
            this.api.getRecords(opts, function (err, result) {
                return err?err:result;
            });
        }
    };
     /**
     * 获取客服基本信息
     * 参数：
     * callbackFun:function,回调函数
     * http请求方式: GET
     * Result
     * {
       {
    "kf_list" : [
       {
          "kf_account" : "test1@test",
          "kf_headimgurl" : "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjfUS8Ym0GSaLic0FD3vN0V8PILcibEGb2fPfEOmw/0",
          "kf_id" : "1001",
          "kf_nick" : "ntest1"
       },
       {
          "kf_account" : "test2@test",
          "kf_headimgurl" : "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjfUS8Ym0GSaLic0FD3vN0V8PILcibEGb2fPfEOmw/0",
          "kf_id" : "1002",
          "kf_nick" : "ntest2"
       },
       {
          "kf_account" : "test3@test",
          "kf_headimgurl" : "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjfUS8Ym0GSaLic0FD3vN0V8PILcibEGb2fPfEOmw/0",
          "kf_id" : "1003",
          "kf_nick" : "ntest3"
       }
    ]
 }
     */
    getCustomServiceList(callBackFun:any){
        if (typeof (callBackFun) == 'function') {
            this.api.getCustomServiceList(callBackFun);
        } else {
            this.api.getCustomServiceList(function (err, result) {
                return err?err:result;
            });
        }
    };
    /**
     * 获取在线客服接待信息
     * 参数：
     * callbackFun:function,回调函数
     * http请求方式: GET
     * Result
     *{
   "kf_online_list": [
       {
           "kf_account": "test1@test", 
           "status": 1, 
           "kf_id": "1001", 
           "auto_accept": 0, 
           "accepted_case": 1
       },
       {
           "kf_account": "test2@test", 
           "status": 1, 
           "kf_id": "1002", 
           "auto_accept": 0, 
           "accepted_case": 2
       }
   ]
}
     */
    getOnlineCustomServiceList(callBackFun:any){
        if (typeof (callBackFun) == 'function') {
            this.api.getOnlineCustomServiceList(callBackFun);
        } else {
            this.api.getOnlineCustomServiceList(function (err, result) {
                return err?err:result;
            });
        }
    };
    /**
     * 添加客服账号
     * 参数：
     * account:string,账号名字，格式为：前缀@公共号名字
     * nickName:String,昵称
     * password:string,密码，可以直接传递明文，wechat模块自动进行md5加密
     * callBackFun:any,回调函数
     * Result:

{
 "errcode" : 0,
 "errmsg" : "ok",
}
     */
    addKfAccount(account:string,nickName:String,password:string,callBackFun:any){
        if (typeof (callBackFun) == 'function') {
            this.api.addKfAccount(account,nickName,password,callBackFun);
        } else {
            this.api.addKfAccount(account,nickName,password,function (err, result) {
                return err?err:result;
            });
        }
    };
    /**
     * 设置客服账号
     * 参数：
     * account:string,账号名字，格式为：前缀@公共号名字
     * nickName:String,昵称
     * password:string,密码，可以直接传递明文，wechat模块自动进行md5加密
     * callBackFun:any,回调函数
     * Result:

{
 "errcode" : 0,
 "errmsg" : "ok",
}
     */
    updateKfAccount(account:string,nickName:String,password:string,callBackFun:any){
        if (typeof (callBackFun) == 'function') {
            this.api.updateKfAccount(account,nickName,password,callBackFun);
        } else {
            this.api.updateKfAccount(account,nickName,password,function (err, result) {
                return err?err:result;
            });
        }
    };
    /**
     * 删除客服账号
     * 参数：
     *account:string,账号名字，格式为：前缀@公共号名字
     * callBackFun:any,回调函数
     * Result:

{
 "errcode" : 0,
 "errmsg" : "ok",
}
     */
    deleteKfAccount(account:string,callBackFun:any){
        if (typeof (callBackFun) == 'function') {
            this.api.deleteKfAccount(account, callBackFun);
        } else {
            this.api.deleteKfAccount(account, function (err, result) {
                return err ? err : result;
            });
        }
    };
    /**
     * 设置客服头像
     * 参数：
     *account:string,账号名字，格式为：前缀@公共号名字
     * filepath:string,头像路径
     */
    setKfAccountAvatar(account:string,filepath:string,callBackFun:any){
        if (typeof (callBackFun) == 'function') {
            this.api.setKfAccountAvatar(account, filepath,callBackFun);
        } else {
            this.api.setKfAccountAvatar(account, filepath, function (err, result) {
                return err ? err : result;
            });
        }
    };
};

//export = WxCustomService;