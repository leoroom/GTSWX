import * as fs from "fs";
var WechatAPI = require("wechat-api");

var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));
/**
 * 微信接口用户管理模块
 */
export class WxUsers {
    /**
    * 参数
    */
    token: string;
    app_id: string;
    app_secret: string;
    openid: string;
    app_encodingAESKey: string;
    params: any;
    api: any;
    constructor(args) {
        if (args) {
            this.params = args;

            if (args.openid) {

                this.openid = args.openid;

            }
        }

        this.app_id = wxconfig.wx.appID;
        this.app_secret = wxconfig.wx.appsecret;
        this.app_encodingAESKey = wxconfig.wx.encodingAESKey;
        this.token = wxconfig.wx.token;
        this.api = new WechatAPI(this.app_id, this.app_secret);
    };
    /**
     *  获取服务号所有关注者，一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
     *result:{
         "total":2,
         "count":2,
         "data":{
           "openid":["","OPENID1","OPENID2"]
         },
         "next_openid":"NEXT_OPENID"
        }
     */
    getFollowers() {

        this.api.getFollowers(function (err, result) {
            if (err) {
                return err;
            } else {
                return result;
            }
        });
    };
    /**
     * 获取服务号下一个10000关注者
     * 参数：next_openid，一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
     * result:
     * {
         "total":2,
         "count":2,
         "data":{
           "openid":["","OPENID1","OPENID2"]
         },
         "next_openid":"NEXT_OPENID"
        }
     */
    getFollowersByNextOpenId(nextopenid: any) {
        if (!nextopenid) {
            return " has invalid nextopenid;";
        }
        this.api.getFollowers(nextopenid, function (err, result) {
            if (err) {
                return err;
            } else {
                return result;
            }
        });
    };
    /**
     * 通过openid获取用户信息，可以设置lang，其中zh_CN 简体，zh_TW 繁体，en 英语。默认为en
     * 参数：openid
     * 参数：lang
     * result：{
     "subscribe": 1,
     "openid": "o6_bmjrPTlm6_2sgVt7hMZOPfL2M",
     "nickname": "Band",
     "sex": 1,
     "language": "zh_CN",
     "city": "广州",
     "province": "广东",
     "country": "中国",
     "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
     "subscribe_time": 1382694957
    }
     */
    getUserInfo(openid: string, lang: string) {
        var queryParam = { openid: openid, lang: lang };
        this.api.getUser(queryParam, function (err, result) {
            if (err) {
                return err;
            } else {
                return result;
            }
        });
    };
    /**
     * 批量获取用户信息，
     * 参数：openid数组['openid1', 'openid2']
     * result:
     * {
      "user_info_list": [{
        "subscribe": 1,
        "openid": "otvxTs4dckWG7imySrJd6jSi0CWE",
        "nickname": "iWithery",
        "sex": 1,
        "language": "zh_CN",
        "city": "Jieyang",
        "province": "Guangdong",
        "country": "China",
        "headimgurl": "http://wx.qlogo.cn/mmopen/xbIQx1GRqdvyqkMMhEaGOX802l1CyqMJNgUzKP8MeAeHFicRDSnZH7FY4XB7p8XHXIf6uJA2SCunTPicGKezDC4saKISzRj3nz/0",
        "subscribe_time": 1434093047,
        "unionid": "oR5GjjgEhCMJFyzaVZdrxZ2zRRF4",
        "remark": "",
        "groupid": 0
      }, {
        "subscribe": 0,
        "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg",
        "unionid": "oR5GjjjrbqBZbrnPwwmSxFukE41U",
      }]
    } 
     */
    batchGetUsers(openid: any) {
        this.api.getUser(openid, function (err, result) {
            if (err) {
                return err;
            } else {
                return result;
            }
        });
    };
    /**
     * 获取分组列表
     * 参数：callbackFun，回调函数
     * result:
     * {
         "groups": [
           {"id": 0, "name": "未分组", "count": 72596},
           {"id": 1, "name": "黑名单", "count": 36}
         ]
        }
     * http请求方式: GET（请使用https协议）
       https://api.weixin.qq.com/cgi-bin/groups/get?access_token=ACCESS_TOKEN
     */
    getGroups(callbackFun){
        if (callbackFun && typeof(callbackFun) == 'function') {
            this.api.getGroups(callbackFun);
        }else{
             this.api.getGroups(function(err,result) {
                 if (err) {
                     return err;
                 }else{
                     return result;
                 }
             });
        }
    };
    /**
     * 查询用户在哪个分组
     * 参数：openid,用户openid
     * callbackFun,回调函数
     * result:
     * {
         "groupid": 102
       }
     * http请求方式: POST（请使用https协议）
        https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=ACCESS_TOKEN
        POST数据格式：json
        POST数据例子：{"openid":"od8XIjsmk6QdVTETa9jLtGWA6KBc"}
     */
    getWhichGroup(openid:string,callbackFun){
        if (callbackFun && typeof(callbackFun) == 'function') {
            this.api.getWhichGroup(callbackFun);
        }else{
             this.api.getWhichGroup(function(err,result) {
                 if (err) {
                     return err;
                 }else{
                     return result;
                 }
             });
        }
    };
    /***
     * 创建分组
     * groupname,分组名称
     * callbackFun,回调函数
     * Result
     * {"group": {"id": 107, "name": "test"}}
     * http请求方式: POST（请使用https协议）
        https://api.weixin.qq.com/cgi-bin/groups/create?access_token=ACCESS_TOKEN
        POST数据格式：json
        POST数据例子：{"group":{"name":"test"}}
     */
    createGroup(groupname:string,callbackFun){
        if (callbackFun && typeof(callbackFun) == 'function') {
            this.api.createGroup(groupname,callbackFun);
        }else{
             this.api.createGroup(groupname,function(err,result) {
                 if (err) {
                     return err;
                 }else{
                     return result;
                 }
             });
        }
    };
    /**
     * 更新分组名字
     * 参数：
     * groupid,分组Id，由微信分配
     * groupname,新的分组名字
     * callbackFun，回调函数
     * http请求方式: POST（请使用https协议）
        https://api.weixin.qq.com/cgi-bin/groups/update?access_token=ACCESS_TOKEN
        POST数据格式：json
        POST数据例子：{"group":{"id":108,"name":"test2_modify2"}}
     */
    updateGroup(groupid:number,groupname:string,callbackFun){
        if (callbackFun && typeof(callbackFun) == 'function') {
            this.api.updateGroup(groupid,groupname,callbackFun);
        }else{
             this.api.updateGroup(groupid,groupname,function(err,result) {
                 if (err) {
                     return err;
                 }else{
                     return result;
                 }
             });
        }
    };
    /**
     * 移动用户进分组
     * 参数：
     * to_groupid,分组Id，由微信分配
     * openid,用户的openid
     * callbackFun，回调函数
     * http请求方式: POST（请使用https协议）
        https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=ACCESS_TOKEN
        POST数据格式：json
        POST数据例子：{"openid":"oDF3iYx0ro3_7jD4HFRDfrjdCM58","to_groupid":108}
     */
    moveUserToGroup(to_groupid:number,openid:string,callbackFun){
        if (callbackFun && typeof(callbackFun) == 'function') {
            this.api.moveUserToGroup(openid,to_groupid,callbackFun);
        }else{
             this.api.moveUserToGroup(openid,to_groupid,function(err,result) {
                 if (err) {
                     return err;
                 }else{
                     return result;
                 }
             });
        }
    };
    /**
     * 删除分组
     * 参数：
     * groupid,分组Id，由微信分配
     * callbackFun，回调函数
     * http请求方式: POST（请使用https协议）
        https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=ACCESS_TOKEN
        POST数据格式：json
        POST数据例子：{"group":{"id":108}}
     */
    removeGroup(groupId:number,callbackFun){
        if (callbackFun && typeof(callbackFun) == 'function') {
            this.api.removeGroup(groupId,callbackFun);
        }else{
             this.api.removeGroup(groupId,function(err,result) {
                 if (err) {
                     return err;
                 }else{
                     return result;
                 }
             });
        }
    };
};

//export = WxUsers;