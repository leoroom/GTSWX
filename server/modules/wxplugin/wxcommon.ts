var WechatAPI = require("wechat-api");
import * as fs from "fs";
import * as db from "../../common/db";

var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));

/**
 * WxCommon,微信接口通用模块
 */
export class WxCommon {
    /**
     * 参数
     */
    isSingleProcess:boolean = false;//是否支持多进程，默认为false，只支持单进程
    token: string;
    app_id: string;
    app_secret: string;
    openid: string;
    app_encodingAESKey: string;
    params: any;
    api:any;
    constructor(args) {
        if (args) {
            this.params = args;

            if (args.openid) {

            this.openid = args.openid;

            }
            if (args.isSingleProcess) {
                this.isSingleProcess = args.isSingleProcess;
            }
        }
        this.app_id             = wxconfig.wx.appID;
        this.app_secret         = wxconfig.wx.appsecret;
        this.app_encodingAESKey = wxconfig.wx.encodingAESKey;
        this.token              = wxconfig.wx.token;
        if (this.isSingleProcess) {
            this.api = new WechatAPI(this.app_id, this.app_secret, function (callback) {
                var access_token = db.collection("sys.WxAccessToken");

                // 传入一个获取全局token的方法
                fs.readFile('access_token.txt', 'utf8', function (err, txt) {
                    if (err) { return callback(err); }
                    callback(null, JSON.parse(txt));
                });
            }, function (token, callback) {
                // 请将token存储到全局，跨进程、跨机器级别的全局，比如写到数据库、redis等
                // 这样才能在cluster模式及多机情况下使用，以下为写入到文件的示例
                fs.writeFile('access_token.txt', JSON.stringify(token), callback);
            });
        } else {
            this.api = new WechatAPI(this.app_id, this.app_secret);
        }
        
    };
    /**
     * 用于设置urllib的默认options
     * 参数：
     * options,默认选项
     * callbackFun,回调函数
     * api.setOpts({timeout: 15000});
     */
    setOpts(options) {
        
        this.api.setOpts(options);
       
     };
    /**
     * 设置urllib的hook，作用待研究
     * 参数：
     * beforeRequest(Function)	需要封装的方法
     * callbackFun,回调函数
     * api.setHook(function (options) {
          // options
        });
     */
    request(options,callbackFun) { 
        this.api.setHook(function(options){

        });
        this.api.beforeRequest(callbackFun);
    };
    /**************
     * 获取最新的token
     * 参数：callbackFun 毁掉函数
     * 
     */
    getLatestToken(callbackFun) {
        if (typeof(callbackFun) === 'function') {
            this.api.getLatestToken(callbackFun);
        }else{
             this.api.getLatestToken(function(err,result) {
                 if (err) {
                     return err;
                 }else{
                     return result;
                 }
             });
        }

     };
     /**
      * 用于支持对象合并。将对象合并到API.prototype上，使得能够支持扩展
       API.mixin(require('./lib/api_media'));
      */
    mixin(obj:any) {
        this.api.mixin(obj);
     };
     /**
      * 获取微信服务器IP地址
      *参数：callbackFun，回调函数
      *result：
          {
              "ip_list":["127.0.0.1","127.0.0.1"]
          }
      */
     getIp(callbackFun){
        if (typeof(callbackFun) === 'function') {
            this.api.getIp(callbackFun);
        }else{
            this.api.getIp(function(err,result){
                return err?err:result;
            });
        }
     };
    /**************
     * 多台服务器负载均衡时，ticketToken需要外部存储共享。需要调用此registerTicketHandle来设置获取和保存的自定义方法。
     * 参数：callbackFun 回调函数
     * 
     */
     registerTicketHandle(getTicketToken, saveTicketToken){
        if (typeof(getTicketToken) === 'function' && typeof(saveTicketToken) === 'function') {
            this.api.registerTicketHandle(getTicketToken, saveTicketToken);
        }else{
            this.api.registerTicketHandle(function(type,callback){
                // settingModel.getItem(type, { key: 'weixin_ticketToken' }, function (err, setting) {
                //     if (err) return callback(err);
                //     callback(null, setting.value);
                // });
            },function(type, _ticketToken, callback){
                // settingModel.setItem(type, { key: 'weixin_ticketToken', value: ticketToken }, function (err) {
                //     if (err) return callback(err);
                //     callback(null);
                // });
            });
        }
     };
     /**
      * 获取js sdk所需的有效js ticket
      *Result:
          errcode, 0为成功
          errmsg, 成功为'ok'，错误则为详细错误信息
          ticket, js sdk有效票据，如：bxLdikRXVbTPdHSM05e5u5sUoXNKd8-41ZO3MhKoyN5OfkWITDGgnr2fwJ0m9E8NYzWKVZvdVtaUgWvsdshFKA
          expires_in, 有效期7200秒，开发者必须在自己的服务全局缓存jsapi_ticket
      */
     getTicket(callbackFun){
         if (typeof (callbackFun) === 'function') {
             this.api.getTicket(callbackFun);
         } else {
             this.api.getTicket(function (err, result) {
                    if (err) {
                        return err;
                    }else{
                        return result;
                    }
             });
         }
     };
    /**************
     * 获取微信JS SDK Config的所需参数。
     * 参数：
     * param,参数对象
     * {
          debug: false,
          jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage'],
          url: 'http://www.xxx.com'
         };
     * callbackFun 回调函数
     * 
     */
     getJsConfig(param, callbackFun){
        if (typeof (callbackFun) === 'function') {
             this.api.getJsConfig(param,callbackFun);
         } else {
             this.api.getJsConfig(param,function (err, result) {
                    if (err) {
                        return err;
                    }else{
                        return result;
                    }
             });
         }
     };
    /**************
     * 获取微信JS SDK Config的所需参数。
     * 参数：
     * param,参数对象
     * {
          debug: false,
          jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage'],
          url: 'http://www.xxx.com'
         };
     * callbackFun 回调函数
     * 
     */
     getCardExt(param, callbackFun){
        if (typeof (callbackFun) === 'function') {
             this.api.getCardExt(param,callbackFun);
         } else {
             this.api.getCardExt(param,function (err, result) {
                    if (err) {
                        return err;
                    }else{
                        return result;
                    }
             });
         }
     };
    /**************
     * 获取最新的js api ticket。
     * 参数：
     * callbackFun 回调函数
     * 
     */
     getLatestTicket(callbackFun){
        if (typeof (callbackFun) === 'function') {
             this.api.getLatestTicket(callbackFun);
         } else {
             this.api.getLatestTicket(function (err, ticket) {
                    if (err) {
                        return err;
                    }else{
                        return ticket;
                    }
             });
         }
     };
};

//export = WxCommon;