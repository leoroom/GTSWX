﻿import db = require("../common/db");
var ObjectId = require("mongodb").ObjectID;

function ISODate(params: string) {
    return new Date(params);
}

export =async function () {
    var users = db.collection("users");
    var roles = db.collection("roles");
    var roleUsers = db.collection("roleUsers");
    var menus = db.collection("menus");
    var enums = db.collection("enums");
    var holidays = db.collection("holidays");
    var serviceType = db.collection("serviceType");
    var role = await roles.findOneAsync({ roleName: "admin" });
    if (!role) {
        var newrole = await roles.insertAsync({ roleName: "admin", status: "1", created: new Date() });
        var adminroleid = newrole.insertedIds[0].toString();
        var newuser = await users.insertAsync({ userName: "admin", passWord: "21232f297a57a5a743894a0e4a801fc3", provider: "system", emails: "qiukai@live.com", status: "1", created: new Date() });
        var adminuserid = newuser.insertedIds[0].toString();
        await roleUsers.insertAsync({ user: adminuserid, role: adminroleid });

        var menulist = [
            {
                "_id": ObjectId("56e6d9a4c939ed9c25df8ebc"),
                "name": "系统设置",
                "seq": "1",
                "type": "group",
                "icon": "",
                "parent": "0",
                "open": "true",
                "updated": ISODate("2016-03-16T14:46:38.423Z")
            }
            ,
            {
                "_id": ObjectId("56e6d9a4c939ed9c25df8ebd"),
                "name": "我的工作间",
                "seq": "0",
                "type": "group",
                "parent": "0",
                "icon": "",
                "created": "2016-03-14T15:32:52.062Z",
                "open": "true",
                "updated": ISODate("2016-03-16T14:46:27.681Z")
            }
            ,
            {
                "_id": ObjectId("56e6d9a4c939ed9c25df8ebf"),
                "name": "Dashboard",
                "seq": "0",
                "url": "dashboard",
                "css": "",
                "icon": "home",
                "details": "MyTodo",
                "type": "menu",
                "parent": "56e6d9a4c939ed9c25df8ebd",
                "created": "2016-03-14T15:32:52.094Z",
                "updated": ISODate("2016-03-21T14:42:21.798Z")
            }

            ,
            {
                "_id": ObjectId("56e6d9a4c939ed9c25df8ec0"),
                "name": "用户管理",
                "seq": "1",
                "url": "system.user",
                "css": "",
                "icon": "user",
                "details": "Users",
                "type": "menu",
                "parent": "56e6d9a4c939ed9c25df8ebc",
                "created": "2016-03-14T15:32:52.095Z",
                "updated": ISODate("2016-03-21T14:46:10.060Z")
            }

            ,
            {
                "_id": ObjectId("56e6d9a4c939ed9c25df8ec1"),
                "name": "角色管理",
                "seq": "2",
                "url": "system.role",
                "css": "",
                "icon": "users",
                "details": "Roles",
                "type": "menu",
                "parent": "56e6d9a4c939ed9c25df8ebc",
                "created": "2016-03-14T15:32:52.096Z",
                "updated": ISODate("2016-03-21T14:47:13.139Z")
            }

            ,
            {
                "_id": ObjectId("56e6d9a4c939ed9c25df8ec2"),
                "name": "菜单管理",
                "seq": "3",
                "url": "system.menu",
                "css": "",
                "icon": "folder-open",
                "details": "Menus",
                "type": "menu",
                "parent": "56e6d9a4c939ed9c25df8ebc",
                "created": "2016-03-14T15:32:52.096Z",
                "updated": ISODate("2016-03-16T14:46:50.497Z")
            }

            ,
            {
                "_id": ObjectId("56e81c343d2f6c2c1b41b304"),
                "name": "服务项目",
                "seq": "2",
                "type": "group",
                "parent": "0",
                "created": "2016-03-15T14:29:08.768Z",
                "open": "true",
                "updated": ISODate("2016-03-16T14:49:45.791Z")
            }

            ,
            {
                "_id": ObjectId("56e81c733d2f6c2c1b41b305"),
                "name": "客户需求",
                "seq": "1",
                "url": "client.customer_requests",
                "css": "",
                "icon": "flag-checkered",
                "details": "",
                "type": "menu",
                "parent": "56e81c343d2f6c2c1b41b304",
                "created": "2016-03-15T14:30:11.275Z",
                "updated": ISODate("2016-05-16T10:30:37.674Z")
            }

            ,
            {
                "_id": ObjectId("56e96efa2d2170b003aa6f3f"),
                "name": "基础信息",
                "seq": "99",
                "type": "group",
                "parent": "0",
                "created": ISODate("2016-03-16T14:34:34.288Z")
            }

            ,
            {
                "_id": ObjectId("56e9700c2d2170b003aa6f41"),
                "name": "客户管理",
                "seq": "1",
                "url": "base.clients",
                "css": "",
                "icon": "coffee",
                "details": "",
                "type": "menu",
                "parent": "56e96efa2d2170b003aa6f3f",
                "created": "2016-03-16T14:39:08.503Z",
                "updated": ISODate("2016-05-25T07:34:50.244Z")
            }

            ,
            {
                "_id": ObjectId("570a1e6d57636888241c1685"),
                "name": "工单指派",
                "seq": "2",
                "url": "client.delegate",
                "css": "",
                "icon": "barcode",
                "details": "",
                "type": "menu",
                "parent": "56e81c343d2f6c2c1b41b304",
                "created": ISODate("2016-04-10T09:35:41.012Z")
            }

            ,
            {
                "_id": ObjectId("570a3b123794c9702182b00e"),
                "name": "我的工单",
                "seq": "3",
                "url": "client.myworkorder",
                "css": "",
                "icon": "bell",
                "details": "",
                "type": "menu",
                "parent": "56e81c343d2f6c2c1b41b304",
                "created": ISODate("2016-04-10T11:37:54.688Z")
            }

            ,
            {
                "_id": ObjectId("570e4b30db51e1c031fc783a"),
                "name": "代码生成",
                "seq": "4",
                "url": "dev.builder",
                "css": "",
                "icon": "bolt",
                "details": "",
                "type": "menu",
                "parent": "56e6d9a4c939ed9c25df8ebc",
                "created": ISODate("2016-04-13T13:35:44.214Z")
            }

            ,
            {
                "_id": ObjectId("570e4c45db51e1c031fc783b"),
                "name": "数据预览",
                "seq": "5",
                "url": "dev.dynamic:table=clients",
                "css": "",
                "icon": "camera",
                "details": "",
                "type": "menu",
                "parent": "56e6d9a4c939ed9c25df8ebc",
                "created": ISODate("2016-04-13T13:40:21.713Z")
            }

            ,
            {
                "_id": ObjectId("573d4c2d937166382ef4eb2e"),
                "name": "报表生成",
                "seq": "4",
                "url": "client.report",
                "css": "",
                "icon": "list",
                "details": "",
                "type": "menu",
                "parent": "56e81c343d2f6c2c1b41b304",
                "created": ISODate("2016-05-19T05:16:29.787Z")
            }

            ,
            {
                "_id": ObjectId("573d4c5a937166382ef4eb2f"),
                "name": "开票",
                "seq": "5",
                "url": "client.invoice",
                "css": "",
                "icon": "usd",
                "details": "",
                "type": "menu",
                "parent": "56e81c343d2f6c2c1b41b304",
                "created": ISODate("2016-05-19T05:17:14.248Z")
            }

            ,
            {
                "_id": ObjectId("57451ba105fbac98111cfbf1"),
                "name": "单价管理",
                "seq": "4",
                "url": "base.price",
                "css": "",
                "icon": "cny",
                "details": "",
                "type": "menu",
                "parent": "56e96efa2d2170b003aa6f3f",
                "created": "2016-05-25T03:27:29.077Z",
                "updated": ISODate("2016-05-25T07:35:06.886Z")
            }

            ,
            {
                "_id": ObjectId("574543a28c30d328101fc2a9"),
                "name": "服务类型",
                "seq": "2",
                "url": "base.serviceType",
                "css": "",
                "icon": "briefcase",
                "details": "",
                "type": "menu",
                "parent": "56e96efa2d2170b003aa6f3f",
                "created": "2016-05-25T06:18:10.978Z",
                "updated": ISODate("2016-05-25T07:35:00.709Z")
            }

            ,
            {
                "_id": ObjectId("5745555f865ffeb4334e5085"),
                "name": "枚举管理",
                "seq": "0",
                "url": "base.enum",
                "css": "",
                "icon": "at",
                "details": "",
                "type": "menu",
                "parent": "56e96efa2d2170b003aa6f3f",
                "created": ISODate("2016-05-25T07:33:51.750Z")
            }

            ,
            {
                "_id": ObjectId("5745558e865ffeb4334e5086"),
                "name": "产品管理",
                "seq": "3",
                "url": "base.product",
                "css": "",
                "icon": "paper-plane",
                "details": "",
                "type": "menu",
                "parent": "56e96efa2d2170b003aa6f3f",
                "created": "2016-05-25T07:34:38.571Z",
                "updated": ISODate("2016-05-25T07:35:04.763Z")
            }

            ,
            {
                "_id": ObjectId("574665dbd75a4ef0198842d2"),
                "name": "节假日管理",
                "seq": "6",
                "url": "base.holiday",
                "css": "",
                "icon": "youtube-play",
                "details": "",
                "type": "menu",
                "parent": "56e96efa2d2170b003aa6f3f",
                "created": ISODate("2016-05-26T02:56:27.713Z")
            }
        ];

        menus.insertMany(menulist);

        var enumlist = [
            {
                "_id": ObjectId("574555b5865ffeb4334e5087"),
                "created": "2016-05-25T07:35:17.156Z",
                "group": "base",
                "updated": ISODate("2016-05-26T05:32:29.840Z"),
                "module": "clientType",
                "ekey": "OEM",
                "evalue": "OEM",
                "productName": ""
            }

            ,
            {
                "_id": ObjectId("57456a27865ffeb4334e5088"),
                "created": "2016-05-25T09:02:31.237Z",
                "group": "base",
                "updated": ISODate("2016-05-25T09:50:55.888Z"),
                "module": "clientType",
                "ekey": "SUPPLIER",
                "evalue": "SUPPLIER"
            }

            ,
            {
                "_id": ObjectId("57456a52865ffeb4334e5089"),
                "created": "2016-05-25T09:03:14.126Z",
                "group": "base",
                "updated": ISODate("2016-05-25T10:22:39.294Z"),
                "module": "billing",
                "ekey": "计件",
                "evalue": "计件"
            }

            ,
            {
                "_id": ObjectId("57456a72865ffeb4334e508a"),
                "created": "2016-05-25T09:03:46.317Z",
                "group": "base",
                "updated": ISODate("2016-05-25T10:22:43.155Z"),
                "module": "billing",
                "ekey": "计时",
                "evalue": "计时"
            }

            ,
            {
                "_id": ObjectId("57456a80865ffeb4334e508b"),
                "created": "2016-05-25T09:04:00.489Z",
                "group": "base",
                "updated": ISODate("2016-05-25T10:24:03.592Z"),
                "module": "engineerbilling",
                "ekey": "包天",
                "evalue": "包天"
            }

            ,
            {
                "_id": ObjectId("57456a92865ffeb4334e508c"),
                "created": "2016-05-25T09:04:18.109Z",
                "group": "base",
                "updated": ISODate("2016-05-25T10:24:05.443Z"),
                "module": "engineerbilling",
                "ekey": "包月",
                "evalue": "包月"
            }

            ,
            {
                "_id": ObjectId("57456ac6865ffeb4334e508d"),
                "created": "2016-05-25T09:05:10.898Z",
                "group": "base",
                "updated": ISODate("2016-05-25T10:31:05.525Z"),
                "module": "productModule",
                "ekey": "显示模块",
                "evalue": "显示模块"
            }

            ,
            {
                "_id": ObjectId("57456c95865ffeb4334e508e"),
                "created": "2016-05-25T09:12:53.729Z",
                "group": "base",
                "updated": ISODate("2016-05-25T10:31:06.191Z"),
                "module": "productModule",
                "ekey": "制动模块",
                "evalue": "制动模块"
            }

            ,
            {
                "_id": ObjectId("57456ca6865ffeb4334e508f"),
                "created": "2016-05-25T09:13:10.803Z",
                "group": "base",
                "updated": ISODate("2016-05-25T10:31:06.842Z"),
                "module": "productModule",
                "ekey": "传动模块",
                "evalue": "传动模块"
            }

            ,
            {
                "_id": ObjectId("57456cc8865ffeb4334e5090"),
                "created": "2016-05-25T09:13:44.958Z",
                "group": "base",
                "updated": ISODate("2016-05-25T10:31:07.473Z"),
                "module": "productModule",
                "ekey": "减震模块",
                "evalue": "减震模块"
            }

            ,
            {
                "_id": ObjectId("57468f92de11d6141dcd7fe3"),
                "created": ISODate("2016-05-26T05:54:26.944Z"),
                "group": "汽车",
                "updated": ISODate("2016-05-26T05:54:40.629Z"),
                "module": "productModule",
                "ekey": "底盘系统",
                "evalue": "底盘系统"
            }

            ,
            {
                "_id": ObjectId("57468fa2de11d6141dcd7fe4"),
                "created": ISODate("2016-05-26T05:54:42.078Z"),
                "group": "汽车",
                "updated": ISODate("2016-05-26T05:54:52.438Z"),
                "module": "productModule",
                "ekey": "内饰",
                "evalue": "内饰"
            }

            ,
            {
                "_id": ObjectId("57468fb1de11d6141dcd7fe5"),
                "created": ISODate("2016-05-26T05:54:57.048Z"),
                "group": "汽车",
                "updated": ISODate("2016-05-26T05:55:04.222Z"),
                "ekey": "动力系统",
                "evalue": "动力系统",
                "module": "productModule"
            }

            ,
            {
                "_id": ObjectId("57468fbdde11d6141dcd7fe6"),
                "created": ISODate("2016-05-26T05:55:09.194Z"),
                "group": "汽车",
                "updated": ISODate("2016-05-26T05:55:19.078Z"),
                "module": "productModule",
                "ekey": "外饰",
                "evalue": "外饰"
            }

            ,
            {
                "_id": ObjectId("57468fc8de11d6141dcd7fe7"),
                "created": ISODate("2016-05-26T05:55:20.437Z"),
                "group": "汽车",
                "updated": ISODate("2016-05-26T05:55:30.531Z"),
                "module": "productModule",
                "ekey": "电子电器系统",
                "evalue": "电子电器系统"
            }

            ,
            {
                "_id": ObjectId("57468fd3de11d6141dcd7fe8"),
                "created": ISODate("2016-05-26T05:55:31.962Z"),
                "group": "汽车",
                "updated": ISODate("2016-05-26T05:55:41.132Z"),
                "module": "productModule",
                "ekey": "白车身",
                "evalue": "白车身"
            }

            ,
            {
                "_id": ObjectId("574818b9a25ad4f031ffdec8"),
                "created": ISODate("2016-05-27T09:51:53.004Z"),
                "group": "HDD",
                "updated": ISODate("2016-05-27T09:53:50.086Z"),
                "module": "productModule",
                "ekey": "PARTS",
                "evalue": "PARTS"
            }

            ,
            {
                "_id": ObjectId("57483c49a25ad4f031ffded1"),
                "created": ISODate("2016-05-27T12:23:37.822Z"),
                "group": "base",
                "updated": ISODate("2016-05-27T12:24:12.742Z"),
                "module": "SortingMethod",
                "ekey": "100%目测",
                "evalue": "100%目测"
            }

            ,
            {
                "_id": ObjectId("57483c6fa25ad4f031ffded2"),
                "created": ISODate("2016-05-27T12:24:15.259Z"),
                "group": "base",
                "updated": ISODate("2016-05-27T12:24:32.919Z"),
                "module": "SortingMethod",
                "ekey": "显微镜",
                "evalue": "显微镜"
            }

            ,
            {
                "_id": ObjectId("57483c82a25ad4f031ffded3"),
                "created": ISODate("2016-05-27T12:24:34.921Z"),
                "group": "base",
                "updated": ISODate("2016-05-27T12:25:22.238Z"),
                "module": "SortingMethod",
                "ekey": "通止规",
                "evalue": "通止规"
            }

            ,
            {
                "_id": ObjectId("57483cb4a25ad4f031ffded4"),
                "created": ISODate("2016-05-27T12:25:24.271Z"),
                "group": "base",
                "updated": ISODate("2016-05-27T12:25:43.462Z"),
                "module": "SortingMethod",
                "ekey": "夹具",
                "evalue": "夹具"
            }
        ];
        enums.insertMany(enumlist);

        var holidayslist = [
            {
                "_id": ObjectId("574665e1d75a4ef0198842d3"),
                "created": ISODate("2016-05-26T02:56:33.043Z"),
                "holiday": "2016-06-09 00:00",
                "updated": ISODate("2016-05-26T02:57:19.302Z"),
                "holidayname": "端午节"
            }

            ,
            {
                "_id": ObjectId("57466688d75a4ef0198842d4"),
                "created": ISODate("2016-05-26T02:59:20.473Z"),
                "holiday": "2016-06-10 00:00",
                "updated": ISODate("2016-05-26T02:59:29.806Z"),
                "holidayname": "端午节"
            }

            ,
            {
                "_id": ObjectId("57466693d75a4ef0198842d5"),
                "created": ISODate("2016-05-26T02:59:31.264Z"),
                "holiday": "2016-06-11 00:00",
                "updated": ISODate("2016-05-26T03:00:04.836Z"),
                "holidayname": "端午节"
            }

            ,
            {
                "_id": ObjectId("574666abd75a4ef0198842d6"),
                "created": ISODate("2016-05-26T02:59:55.045Z"),
                "holiday": "2016-09-15 00:00",
                "updated": ISODate("2016-05-26T03:00:27.126Z"),
                "holidayname": "中秋节"
            }

            ,
            {
                "_id": ObjectId("574666cdd75a4ef0198842d7"),
                "created": ISODate("2016-05-26T03:00:29.609Z"),
                "holiday": "2016-09-16 00:00",
                "updated": ISODate("2016-05-26T03:01:55.647Z"),
                "holidayname": "中秋节"
            }

            ,
            {
                "_id": ObjectId("574666d1d75a4ef0198842d8"),
                "created": ISODate("2016-05-26T03:00:33.005Z"),
                "holiday": "2016-09-17 00:00",
                "updated": ISODate("2016-05-26T03:00:51.321Z"),
                "holidayname": "中秋节"
            }

            ,
            {
                "_id": ObjectId("574666e6d75a4ef0198842d9"),
                "created": ISODate("2016-05-26T03:00:54.189Z"),
                "holiday": "2016-10-01 00:00",
                "updated": ISODate("2016-05-26T03:01:18.764Z"),
                "holidayname": "国庆节"
            }

            ,
            {
                "_id": ObjectId("574666e7d75a4ef0198842da"),
                "created": ISODate("2016-05-26T03:00:55.500Z"),
                "holiday": "2016-10-02 00:00",
                "updated": ISODate("2016-05-26T03:01:23.492Z"),
                "holidayname": "国庆节"
            }

            ,
            {
                "_id": ObjectId("574666e7d75a4ef0198842db"),
                "created": ISODate("2016-05-26T03:00:55.693Z"),
                "holiday": "2016-10-03 00:00",
                "updated": ISODate("2016-05-26T03:01:28.277Z"),
                "holidayname": "国庆节"
            }

            ,
            {
                "_id": ObjectId("574666e7d75a4ef0198842dc"),
                "created": ISODate("2016-05-26T03:00:55.960Z"),
                "holiday": "2016-10-04 00:00",
                "updated": ISODate("2016-05-26T03:01:32.416Z"),
                "holidayname": "国庆节"
            }

            ,
            {
                "_id": ObjectId("574666ead75a4ef0198842dd"),
                "created": ISODate("2016-05-26T03:00:58.692Z"),
                "holiday": "2016-10-05 00:00",
                "updated": ISODate("2016-05-26T03:01:55.137Z"),
                "holidayname": "国庆节"
            }

            ,
            {
                "_id": ObjectId("574666f6d75a4ef0198842de"),
                "created": ISODate("2016-05-26T03:01:10.055Z"),
                "holiday": "2016-10-06 00:00",
                "updated": ISODate("2016-05-26T03:01:44.911Z"),
                "holidayname": "国庆节"
            }

            ,
            {
                "_id": ObjectId("574666f6d75a4ef0198842df"),
                "created": ISODate("2016-05-26T03:01:10.516Z"),
                "holiday": "2016-10-07 00:00",
                "updated": ISODate("2016-05-26T03:01:53.245Z"),
                "holidayname": "国庆节"
            }
        ];
        holidays.insertMany(holidayslist);

        var serviceTypelist = [
            {
                "_id": ObjectId("574543bc8c30d328101fc2aa"),
                "created": ISODate("2016-05-25T06:18:36.651Z"),
                "serviceType": "Sorting",
                "updated": ISODate("2016-05-25T06:18:43.668Z")
            }

            ,
            {
                "_id": ObjectId("574543f28c30d328101fc2ab"),
                "created": "2016-05-25T06:19:30.067Z",
                "serviceType": "Rework",
                "updated": ISODate("2016-05-25T06:19:57.427Z")
            }

            ,
            {
                "_id": ObjectId("574544198c30d328101fc2ac"),
                "created": "2016-05-25T06:20:09.612Z",
                "serviceType": "Repair",
                "updated": ISODate("2016-05-25T06:20:30.658Z")
            }

            ,
            {
                "_id": ObjectId("574544448c30d328101fc2ad"),
                "created": ISODate("2016-05-25T06:20:52.213Z"),
                "serviceType": "IPQC",
                "updated": ISODate("2016-05-25T06:21:07.363Z")
            }

            ,
            {
                "_id": ObjectId("5745445a8c30d328101fc2ae"),
                "created": ISODate("2016-05-25T06:21:14.439Z"),
                "serviceType": "整车排查",
                "updated": ISODate("2016-05-25T06:21:17.653Z")
            }

            ,
            {
                "_id": ObjectId("57457b67ccf66a1826612c3c"),
                "created": "2016-05-25T10:16:07.005Z",
                "serviceType": "测量",
                "updated": ISODate("2016-05-25T10:16:48.967Z")
            }

            ,
            {
                "_id": ObjectId("57457b92ccf66a1826612c3d"),
                "created": ISODate("2016-05-25T10:16:50.750Z"),
                "serviceType": "试验",
                "updated": ISODate("2016-05-25T10:16:55.793Z")
            }

            ,
            {
                "_id": ObjectId("57457b97ccf66a1826612c3e"),
                "created": ISODate("2016-05-25T10:16:55.788Z"),
                "serviceType": "FAE",
                "updated": ISODate("2016-05-25T10:16:58.605Z")
            }
        ];
        serviceType.insertMany(serviceTypelist);
        
    }

}