﻿import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
import * as plugins from "../../common/plugins"
var router = express.Router();
import * as fs from 'fs';
import * as path from 'path';
import * as moment from 'moment';
var ejsExcel = require('ejsexcel');

var RootPath = process.env.RootPath;
var TempPath = process.env.TempPath;
var PicPath = process.env.PicPath;
var DocPath = process.env.DocPath;

//#region 增删改查
var items = db.collection("workorders");
var details = db.collection("workdetail");
router.get("/", async (req: express.Request, res: express.Response, next) => {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/pager", plugins.webix_all("workorders"));

function movefiles(source: string, target: string) {
    if (!source) return "";
    var imgs = source.split(',');
    return imgs.map(img => movefile(img, target)).join(',');
}

function movefile(source: string, target: string) {
    if (source != null && source.length > 0) {
        var sourceFile = path.join(RootPath + TempPath, source);
        var file = source;
        var filename = file.substring(0, file.lastIndexOf("."));
        var ext = file.substr(file.lastIndexOf("."));
        var newname = filename + ext;
        var newpath = "/" + target + "/" + newname;
        var destPath = path.join(RootPath, newpath);

        if (!fs.existsSync(sourceFile) && fs.existsSync(destPath)) {
            return source;
        }

        var fileindex = 0;
        while (fs.existsSync(destPath)) {
            fileindex++;
            newname = filename + "(" + fileindex + ")" + ext;
            newpath = "/" + target + "/" + newname;
            destPath = path.join(RootPath, newpath);
        }

        var dir = path.join(RootPath, target);
        try {
            var info = fs.statSync(dir);
            if (!info.isDirectory()) {
                fs.mkdirSync(dir);
            }
        } catch (error) {
            fs.mkdirSync(dir);
        }
        try {
            fs.renameSync(sourceFile, destPath);
            return newname;
        } catch (error) {
            console.error(error);
            return null;
        }
    }
}

router.post("/", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    obj.created = new Date();
    var user = req.session["user"];
    obj.creatorId = user.id;
    obj.creator = user.userName;
    obj.sip = movefile(obj.sip, "docs");
    obj.sippath = "/" + DocPath + obj.sip;
    if (obj.pricetable)
        obj.pricetable = JSON.parse(obj.pricetable);
    delete obj.ch;
    // 把<b>标签去掉
    obj.contactsOnJobDisplay = obj.contactsOnJobDisplay.replace("<b>", "").replace("</b>", "");
    obj.contactsDisplay = obj.contactsDisplay.replace("<b>", "").replace("</b>", "");
    //end
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    obj.sip = movefile(obj.sip, "docs");
    obj.sippath = "/" + DocPath + obj.sip;
    if (obj.pricetable)
        obj.pricetable = JSON.parse(obj.pricetable);
    delete obj.ch;
    delete obj.id;
    // 把<b>标签去掉
    obj.contactsOnJobDisplay = obj.contactsOnJobDisplay.replace("<b>", "").replace("</b>", "");
    obj.contactsDisplay = obj.contactsDisplay.replace("<b>", "").replace("</b>", "");
    //end
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var detailsrecord = await details.removeAsync({ requestid: id });
    var record = await items.removeByIdAsync(id);
    res.send({});
});

router.get("/data/pager", plugins.webix_all("workorders", true, {
    "serviceType": 1,
    "vendorCode": 1,
    "vendorType": 1,
    "vendorAddress": 1,
    "contacts": 1,
    "jobAddress": 1,
    "contactsOnJob": 1,
    "arrivalDate": 1,
    "quantityInspectors": 1,
    "sortingCause": 1,
    "remark": 1,
    "suppliesList": 1,
    "billing": 1,
    "engineerbilling": 1,
    "engineerCosts": 1,
    "normalWorkingCosts": 1,
    "overTimeWorkingCosts": 1,
    "weekendWorkingCosts": 1,
    "holidayWorkingCosts": 1,
    "buySpecialToolsCosts": 1,
    "logisticsCosts": 1,
    "spaceRentalFee": 1,
    "accommodationCosts": 1,
    "transportationStandardCosts": 1,
    "otherFee": 1,
    "requestRemark": 1,
    "clientName": 1,
    "contactsDisplay": 1,
    "contactsOnJobDisplay": 1,
    "status": 1,
    "created": 1,
    "creatorId": 1,
    "creator": 1,
    "processorId": 1,
    "processor": 1,
    "processorRemark": 1
}));

router.get("/creator", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var datas = await items.find({ creatorId: user.id }).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/creator/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.creatorId = user.id;
    req.query.and = filter;
    next();
}, plugins.webix_all("workorders"));

router.get("/me", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var datas = await items.find({ processorId: user.id }).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/me/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.processorId = user.id;
    req.query.and = filter;
    next();
}, plugins.webix_all("workorders"));

router.get("/:requestid/detail", async (req: express.Request, res: express.Response, next) => {
    var requestid = req.param("requestid");

    if (!req.query.begindate) {
        var workdetails = await details.find({ requestid: requestid }).toArrayAsync();
        _.each(workdetails, row => {
            row.id = row._id;
            delete row._id;
        });
        res.send(workdetails);
    } else {
        var begindate = moment(req.query.begindate + " 00:00:00", "MM/DD/YYYY HH:mm:ss").toDate();
        var enddate = moment(req.query.enddate + " 23:59:59", "MM/DD/YYYY HH:mm:ss").toDate();
        var workdetails = await details.find({ requestid: requestid, inspectordate: { $gte: begindate, $lte: enddate } }).toArrayAsync();
        _.each(workdetails, row => {
            row.id = row._id;
            delete row._id;
            row.inspectionPersonnelNum = _.uniq(row.steps.map(p => p.inspectors)).length;
            //row.productNumber = _.uniq(row.steps.map(p => p.productNumber)).join(',');
            row.batches = _.uniq(row.steps.map(p => p.batches)).join(',');
        });
        res.send(workdetails);
    }

});
router.get("/:requestid/expert", async (req: express.Request, res: express.Response, next) => {

    var requestid = req.param("requestid");
    var data = await items.findByIdAsync(requestid);

    var contacts = data.contactsDisplay.split(",");

    if (contacts.length >= 1)
        data.contactsDisplay1 = contacts[0].replace("<b>", " ").replace("</b>", " ");
    if (contacts.length >= 2)
        data.contactsDisplay2 = contacts[1].replace("<b>", " ").replace("</b>", " ");

    var contactsOnJobDisplay = data.contactsOnJobDisplay.split(",");
    if (contactsOnJobDisplay.length >= 1)
        data.contactsOnJobDisplay1 = contactsOnJobDisplay[0].replace("<b>", " ").replace("</b>", " ");

    var processor = await db.collection("users").findByIdAsync(data.processorId);
    if (processor)
        data.processorDisplay = processor.userName + " " + processor.tel;

    var begindate = moment(req.query.begindate + " 00:00:00", "MM/DD/YYYY HH:mm:ss").toDate();
    var enddate = moment(req.query.enddate + " 23:59:59", "MM/DD/YYYY HH:mm:ss").toDate();
    var workdetails = await details.find({ requestid: requestid, inspectordate: { $gte: begindate, $lte: enddate } }).toArrayAsync();

    var chart: any = {};
    chart.allcount = _.sumBy(workdetails, "allcount");
    chart.ds1 = [];
    chart.ds2 = [];
    chart.ds3 = [];
    var badReasons = _.chain(workdetails).map("badDetails").flattenDeep().map("badReason").uniq().value();

    _.each(workdetails, row => {
        row.id = row._id;
        delete row._id;
        row.inspectionPersonnelNum = _.uniq(row.steps.map(p => p.inspectors)).length;
        var batches = _.uniq(row.steps.map(p => p.batches));
        row.batches = batches.join(',');
        row.inspectordate = moment(row.inspectordate).format("MM/DD/YYYY");

        row.badImages = row.badImages.split(",").map(img => path.join(PicPath, img));
        row.goodImages = row.goodImages.split(",").map(img => path.join(PicPath, img));
        row.fixImages = row.fixImages.split(",").map(img => path.join(PicPath, img));

        _.each(batches, batch => {
            var allcount = _.chain(row.steps).filter({ batches: batch }).map((p: any) => { return parseInt(p.count) }).sum().value();
            var badcount = _.chain(row.steps).filter({ batches: batch }).map((p: any) => { return parseInt(p.badcount) }).sum().value();
            var badinfo = {
                date: row.inspectordate,
                batch: batch,
                allcount: allcount,
                goodcount: allcount - badcount,
                yieldRate: (allcount - badcount) / allcount
            };
            _.each(badReasons, (badReason, key) => {
                var dabdetail: any = _.find(row.badDetails, { badLot: batch, badReason: badReason });
                if (dabdetail) {
                    badinfo["badReason" + key] = parseInt(dabdetail.badQuantity);
                    badinfo["badReasonRate" + key] = dabdetail.badQuantity / allcount;
                }
            });
            chart.ds2.push(badinfo);
        });

    });

    var goodcount = _.chain(chart.ds2).sumBy("goodcount").value();
    chart.ds1.push({
        label: "合格品",
        Quantity: goodcount,
        Rate: goodcount / chart.allcount
    });
    _.each(badReasons, (badReason, key) => {
        var val = _.chain(chart.ds2).sumBy("badReason" + key).value();
        var totalitem: any = {
            label: badReason,
            Quantity: val,
            Rate: val / chart.allcount
        }
        chart.ds1.push(totalitem);
        chart.ds3.push(badReason);
        chart.ds3.push(badReason + "比例");
    });


    data.steps = workdetails;
    data.chart = chart;


    var sourceFile = path.join(__dirname, "../../../template", "GTS daily report New.xlsx");
    var exlBuf = fs.readFileSync(sourceFile);
    ejsExcel.renderExcelCb(exlBuf, data, function (exlBuf2) {
        res.setHeader('Content-Type', 'application/vnd.openxmlformats');
        res.setHeader("Content-Disposition", "attachment; filename=" + encodeURI("GTS daily report " + moment().format("YYYY_MM_DD") + ".xlsx"));
        res.end(exlBuf2);
    });

});
router.post("/:requestid/detail", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    var requestid = req.param("requestid");
    obj.requestid = requestid;
    obj.created = new Date();
    obj.inspectordate = moment(obj.inspectordate, "YYYY-MM-DD HH:mm").toDate();;
    obj.badImages = movefiles(obj.badImages, "imgs");
    obj.goodImages = movefiles(obj.goodImages, "imgs");
    obj.fixImages = movefiles(obj.fixImages, "imgs");
    if (obj.steps != null && obj.steps.length > 0)
        obj.steps = JSON.parse(obj.steps);
    if (obj.badDetails != null && obj.badDetails.length > 0)
        obj.badDetails = JSON.parse(obj.badDetails);
    delete obj.ch;
    var record = await details.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:requestid/detail/:id/", async (req: express.Request, res: express.Response, next) => {
    var requestid = req.param("requestid");
    var id = req.param("id");
    var obj = req.body;
    obj.inspectordate = moment(obj.inspectordate, "YYYY-MM-DD HH:mm").toDate();;
    obj.updated = new Date();
    obj.badImages = movefiles(obj.badImages, "imgs");
    obj.goodImages = movefiles(obj.goodImages, "imgs");
    obj.fixImages = movefiles(obj.fixImages, "imgs");
    if (obj.steps != null && obj.steps.length > 0)
        obj.steps = JSON.parse(obj.steps);
    if (obj.badDetails != null && obj.badDetails.length > 0)
        obj.badDetails = JSON.parse(obj.badDetails);
    delete obj.ch;
    delete obj.id;
    var record = await details.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:requestid/detail/:id", async (req: express.Request, res: express.Response, next) => {
    var requestid = req.param("requestid");
    var id = req.param("id");
    var record = await details.removeByIdAsync(id);
    res.send({});
});
async function createInvoice(requestid: string) {
    var request = await items.findByIdAsync(requestid);
    var vendors = db.collection("vendors");
    var vendor = await vendors.findOneAsync({ vendorCode: request.vendorCode });
    var workdetails = await details.find({ requestid: requestid }).toArrayAsync();
    var holidays = db.collection("holidays");
    var holidayslst = await holidays.find().toArrayAsync();
    var contactid = parseInt(request.contacts.split(',')[0]);
    var contact = _.find(vendor.contacts, { id: contactid });
    var header = {
        refno: "GTS_" + vendor.vendorName + moment(request.arrivalDate).format("YYYYMMDD"),
        companyName: vendor.vendorName,
        clientAddress: vendor.vendorAddress,
        issueDate: request.arrivalDate,
        jobAddress: request.jobAddress,
        contact: contact["contactName"],
        tel: contact["tel"],
        email: contact["email"],
        billing: request.billing
    }
    var body = [];
    for (var index in workdetails) {
        var detail = workdetails[index];

        if (request.billing == "计件") {
            var unitprice = _.find(request.pricetable, { productName: detail.productName, productNumber: detail.productNumber });
            var price = unitprice ? parseFloat(unitprice["pnPrice"]) : NaN;
            var item = {
                id: detail._id,
                workinglocation: request.jobAddress,
                date: moment(detail.inspectordate).format("YYYY/MM/DD"),
                pn: detail.productNumberName,
                workingcontent: request.serviceType,
                sortingqty: detail.allcount,
                unitprice: price,
                amount: (detail.allcount * price).toFixed(2)
                //transportationfee: request.transportationStandardCosts
            }
            body.push(item);
        } else {

            var inspectordate = moment(detail.inspectordate);
            var inspectordatestr = inspectordate.format("YYYY-MM-DD");
            var beginTime = moment(inspectordatestr + " " + detail.beginTime, "YYYY-MM-DD HH:mm");
            var endTime = moment(inspectordatestr + " " + detail.endTime, "YYYY-MM-DD HH:mm");
            if (beginTime > endTime) {
                endTime.add(1, 'day');
            }
            var holidaysbuf = holidayslst.map(function (item) {
                return moment(item.holiday).format("YYYY-MM-DD");
            })
            var holidayindex = _.indexOf(holidaysbuf, inspectordatestr)
            var isholiday = holidayindex > 0;
            var weekday = inspectordate.day();
            var hours = endTime.diff(beginTime, "hour", true);

            if (hours >= 10)
            { hours = hours - 2; }
            else if (hours >= 5)
            { hours--; }
            hours = Math.round(hours / 0.5) / 2;
            var outTimeJob = 0;
            var holidaysTimeJob = 0;
            var weekendTimeJob = 0;
            var timeJob = 0;
            if (isholiday) {
                holidaysTimeJob = hours;
            } else if (weekday == 6 || weekday == 7) {
                weekendTimeJob = hours;
            } else if (hours > 8) {
                outTimeJob = hours - 8;
                timeJob = 8;
            } else {
                timeJob = hours;
            }
            var totalmanpowerengineer = detail.steps.length;
            var str1 = parseInt(detail.beginTime.split(":")[0]) < 12 ? "D " : "N ";
            var str2 = isholiday ? "国假" : (weekday == 6 ? "Sat." : weekday == 7 ? "Sun." : "");
            var item2 = {
                id: detail._id,
                workinglocation: request.jobAddress,
                date: inspectordatestr + str1 + str2,
                workingplace: "",
                workingcontent: request.serviceType,
                workinghours: detail.beginTime + " - " + detail.endTime,
                normalworkinghours: timeJob,
                totalmanpowerengineer: detail.steps.length,
                unitprice1: request.normalWorkingCosts,
                overtimeworkinghours: outTimeJob + weekendTimeJob + holidaysTimeJob,
                unitprice2: request.overTimeWorkingCosts,
                amount: (totalmanpowerengineer * timeJob * request.normalWorkingCosts +
                    totalmanpowerengineer * outTimeJob * request.overTimeWorkingCosts +
                    totalmanpowerengineer * weekendTimeJob * request.weekendWorkingCosts +
                    totalmanpowerengineer * holidaysTimeJob * request.holidayWorkingCosts
                ).toFixed(2)
                //transportationfee: request.transportationStandardCosts
            }
            body.push(item2);
        }

    }

    var maxDate = _.maxBy(workdetails, "inspectordate");
    var minDate = _.minBy(workdetails, "inspectordate");
    var days = (moment(maxDate).diff(moment(minDate), "days"));
    if (days === 0) days = 1;
    var months = _.ceil(moment(maxDate).diff(moment(minDate), "months", true));
    if (months === 0) months = 1;
    var maxengineersNum = _.maxBy(workdetails, 'engineersNum')["engineersNum"];
    var fae = 0;
    var faestr = '';
    if (request.engineerbilling === "包天") {
        var allengineersNum = _.sumBy(workdetails, "engineersNum");
        faestr = days + " days " + maxengineersNum + " engineer/day";
        fae = maxengineersNum * request.engineerCosts;
    } else if (request.engineerbilling === "包月") {
        faestr = months + " months " + maxengineersNum + " engineer";
        fae = 1 * maxengineersNum * months * request.engineerCosts;
    }
    var accommodationCosts = 1 * days * request.accommodationCosts;
    var accommodationCostsStr = days + " days * " + request.accommodationCosts;

    var transportationCosts = 1 * days * request.transportationStandardCosts;
    var transportationCostsStr = days + " days * " + request.transportationStandardCosts;

    var buySpecialToolsCosts = 1 * request.buySpecialToolsCosts;
    var logisticsCosts = 1 * request.logisticsCosts;
    var spaceRentalFee = 1 * request.spaceRentalFee;
    var otherFee = 1 * request.otherFee;
    var remark = request.requestRemark;

    var total = _.sum(body.map(x => parseFloat(x.amount)));
    var totalamount = total + transportationCosts + accommodationCosts + fae
        + buySpecialToolsCosts + logisticsCosts + spaceRentalFee + otherFee;

    var footer = {
        transportationCosts: transportationCosts,
        transportationCostsStr: transportationCostsStr,
        accommodationCostsStr: accommodationCostsStr,
        accommodationCosts: accommodationCosts,
        buySpecialToolsCosts: buySpecialToolsCosts,
        logisticsCosts: logisticsCosts,
        spaceRentalFee: spaceRentalFee,
        otherFee: otherFee,
        remark: remark,
        FAEcost: fae,
        FAE: faestr,
        total: total.toFixed(2),
        totalamount: totalamount.toFixed(2),
        tax: (totalamount * 0.06).toFixed(2),
        totalamountincludetax: (totalamount * 1.06).toFixed(2),
    };
    return { header: header, body: body, footer: footer };
}
router.get("/:requestid/invoice", async (req: express.Request, res: express.Response, next) => {
    var requestid = req.param("requestid");
    var rs = await createInvoice(requestid);
    res.send(rs);

});

router.get("/:requestid/exportinvoice", async (req: express.Request, res: express.Response, next) => {
    var requestid = req.param("requestid");
    var rs = await createInvoice(requestid);
    var sourceFile;
    if (rs.header.billing === "计件") {
        sourceFile = path.join(__dirname, "../../../template", "invoice A.xlsx");
    } else if (rs.header.billing === "计时") {
        sourceFile = path.join(__dirname, "../../../template", "invoice B.xlsx");
    }

    var exlBuf = fs.readFileSync(sourceFile);
    ejsExcel.renderExcelCb(exlBuf, rs, function (exlBuf2) {
        res.setHeader('Content-Type', 'application/vnd.openxmlformats');
        res.setHeader("Content-Disposition", "attachment; filename=" + encodeURI(rs.header.refno + ".xlsx"));
        res.end(exlBuf2);
    });
});
export = router;
