import * as express from "express";
import * as myutil from "../../common/util";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
var ObjectID = require("mongodb").ObjectID;

var tableName = "pqm.templates";
//#region 增删改查
var items = db.collection(tableName);

router.get("/", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var datas = await items.find({ vendorCode: user.vendorCode }).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/all/pager",async (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    var vendor = await  db.collection("vendors").findOneAsync({ vendorCode: user.vendorCode });
    var suppliers= vendor.suppliers.map(s=>{return s.supplierCode});
    filter.vendorCode = {"$in":suppliers};
    req.query.and = filter;
    next();
}, plugins.webix_all(tableName, true, {
    "vendorCode": 1,
    "vendorName": 1,
    "productName": 1,
    "productNumber": 1,
    "templateCode": 1,
    "templateName": 1,
    "version": 1,
    "status":1
}));
router.get("/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.vendorCode = user.vendorCode;
    req.query.and = filter;
    next();
}, plugins.webix_all(tableName, true, {
    "productName": 1,
    "productNumber": 1,
    "templateCode": 1,
    "templateName": 1,
    "version": 1,
    "status":1
}));
router.get("/publish/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.vendorCode = user.vendorCode;
    filter.status="已发布";
    req.query.and = filter;
    next();
}, plugins.webix_all(tableName, true, {
    "productName": 1,
    "productNumber": 1,
    "templateCode": 1,
    "templateName": 1,
    "version": 1,
    "status":1
}));
router.get("/all/publish/pager",async (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    var vendor = await  db.collection("vendors").findOneAsync({ vendorCode: user.vendorCode });
    var suppliers= vendor.suppliers.map(s=>{return s.supplierCode});
    filter.vendorCode = {"$in":suppliers};
     filter.status="已发布";
    req.query.and = filter;
    next();
}, plugins.webix_all(tableName, true, {
    "vendorCode": 1,
    "vendorName": 1,
    "productName": 1,
    "productNumber": 1,
    "templateCode": 1,
    "templateName": 1,
    "version": 1,
    "status":1
}));
router.get("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.params.id;
    var user = req.session["user"];
    var row = await items.findOneAsync({ _id: new ObjectID(id), vendorCode: user.vendorCode });

    row.id = row._id;
    row.process = myutil.Tree(row.process);
    delete row._id;

    res.send(row);
});
router.post("/", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    if (obj.contacts)
        obj.contacts = JSON.parse(obj.contacts);
    var user = req.session["user"];
    obj.vendorCode = user.vendorCode;
    obj.vendorName = user.vendorName;
    if (obj.process)
        obj.process = JSON.parse(obj.process);
    if (obj.processDetail)
        obj.processDetail = JSON.parse(obj.processDetail);
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    if (obj.contacts)
        obj.contacts = JSON.parse(obj.contacts);
    var user = req.session["user"];
    obj.vendorCode = user.vendorCode;
    obj.vendorName = user.vendorName;
    if (obj.process)
        obj.process = JSON.parse(obj.process);
    if (obj.processDetail)
        obj.processDetail = JSON.parse(obj.processDetail);
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#endregion

export = router;
