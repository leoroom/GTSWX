import * as express from "express";
import * as myutil from "../../common/util";
import db = require("../../common/db");
import _ = require("lodash");
import moment = require("moment");
var xlsx = require('node-xlsx');
var router = express.Router();
import * as plugins from "../../common/plugins";
var ObjectID = require("mongodb").ObjectID;

async function loadSPCDatas(req: express.Request) {
    var productName = req.query.productName;
    var productNumber = req.query.productNumber;
    var workorder = req.query.workorder;
    var partNumber = req.query.partNumber;
    var equipmentType = req.query.equipmentType;
    var equipment = req.query.equipment;
    var mode = req.query.mode;
    var parameter = req.query.parameter;

    var user = req.session["user"];
    var xAxisData = [];
    var seriesData = [];
    var rs: any = {};
    var filter: any = { vendorCode: user.vendorCode };
    if (mode == "equipmentQuery") {

        if (equipment) {
            filter.equipment = equipment;
        } else {
            var equipments = await db.collection("equipments").find({ equipmentType: equipmentType, vendorCode: user.vendorCode }).toArrayAsync();
            filter.equipment = { $in: equipments.map(e => { return e.equipmentCode }) };
        }
        filter.parameter = parameter;

    } else {
        filter.productName = productName;
        filter.productNumber = productNumber;
        if (workorder) {
            var r = new RegExp(workorder, "i")
            filter.workorder = r;
        }
        if (partNumber) {
            var r = new RegExp(partNumber, "i")
            filter.partNumber = r;
        }
    }

    var Parameters = await db.collection("pqm.ProcessParameter").find(filter).sort({ time: 1 }).toArrayAsync();
    for (var index = 0; index < Parameters.length; index++) {
        var element = Parameters[index];
        //await db.collection("pqm.keyin").findOneAsync({ vendorCode: user.vendorCode,pn:element.pn})
        // var row: any = {
        //     productName: element.productName,
        //     productNumber: element.productNumber,
        //     workorder: element.workorder,
        //     partNumber: element.partNumber,
        //     process: element.process,
        //     equipment: element.equipment,
        //     parameter: element.parameter,
        //     nvalue: element.nvalue,
        //     value: element.value,
        //     user: element.user,
        //     time: element.time
        // }
        if (!rs.uvalue && element.uvalue) {
            rs.uvalue = parseFloat(element.uvalue),
                rs.nvalue = parseFloat(element.nvalue),
                rs.lvalue = parseFloat(element.lvalue),
                rs.tuvalue = parseFloat(element.tuValue),
                rs.tlvalue = parseFloat(element.tlValue),
                rs.equipment = element.equipment;
        }
        xAxisData.push(moment(element.time).format("YYYY-MM-DD HH:mm"));
        seriesData.push(parseFloat(element.value));
        rs.max = _.max([_.max(seriesData), rs.tuvalue]) * 1.05;
        rs.min = _.min([_.min(seriesData), rs.lvalue]) * 0.95;
    }
    rs.xAxisData = xAxisData;
    rs.seriesData = seriesData;
    return rs;
}
router.get("/spc", async (req: express.Request, res: express.Response, next) => {
    var rs = await loadSPCDatas(req);
    res.send(rs);
});

router.get("/getParameters/:equipmentType", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var equipmentType = req.params.equipmentType;
    var equipments = await db.collection("equipments").find({ equipmentType: equipmentType, vendorCode: user.vendorCode }).toArrayAsync();
    var filter: any = { vendorCode: user.vendorCode };
    filter.equipment = { $in: equipments.map(e => { return e.equipmentCode }) };

    var ps = await db.collection("pqm.ProcessParameter").find(filter, { parameter: 1 }).toArrayAsync();
    var parameter = _.uniq(ps.map(p => { return p.parameter }));
    res.send(parameter);
});

async function loadYieldDatas(req: express.Request) {
    var productName = req.query.productName;
    var productNumber = req.query.productNumber;
    var workorder = req.query.workorder;
    var hastime = req.query.beginTime != "null";
    hastime = hastime && req.query.endTime != "null";
    var beginTime, endTime;
    if (hastime) {
        beginTime = moment(new Date(req.query.beginTime));
        endTime = moment(new Date(req.query.endTime)).add(1, "day");
    }

    var user = req.session["user"];
    var templates = db.collection("pqm.templates");
    var keyins = db.collection("pqm.keyin");
    var mytemplates = await templates.find({ vendorCode: user.vendorCode, productName: productName, productNumber: productNumber }).toArrayAsync();
    var templateCodes = mytemplates.map(t => { return t.templateCode });
    var mykeyins = await keyins.find({ vendorCode: user.vendorCode, templateCode: { $in: templateCodes } }).toArrayAsync();

    //rs.OutputMin,rs.OutputMax,rs.OutputInterval,rs.YieldMin,rs.YieldMax,rs.Yieldnterval,rs.xAxisData,rs.xAxisData,rs.OutputSeriesData,rs.YieldSeriesData
    var rs: any = {};
    var xAxisData = [];
    var OutputSeriesData = [];
    var YieldSeriesData = [];
    mykeyins.forEach(keyin => {
        if (keyin.parent == 0) return;
        var parent: any = _.find(mykeyins, { _id: new ObjectID(keyin.parent) });
        if (!parent) return;
        if (workorder) {
            var r = new RegExp(workorder, "i")
            if (!r.test(parent.pn)) return;
        }
        if (keyin.process.length == 0) return;
        if (keyin.status != "已完成") return;

        var beginProcess: any = _.head(keyin.process);
        var endProcess: any = _.last(keyin.process);
        if (!beginProcess.form) return;
        if (!endProcess.form) return;

        var incount = parseInt(beginProcess.form.incount);
        if (incount == 0) return;
        var outcount = parseInt(endProcess.form.outcount);
        if (outcount == 0) return;
        var badratio = outcount / incount;
        xAxisData.push(moment(beginProcess.form.beginTime).format("YYYY-MM-DD HH:mm"));
        OutputSeriesData.push({
            value: outcount,
            workorderid:parent._id,
            workorder: parent.pn
        });
        YieldSeriesData.push(badratio);
    });

    rs.OutputMin = _.min(OutputSeriesData);
    rs.OutputMax = _.max(OutputSeriesData);
    rs.OutputMin = (rs.OutputMax - rs.OutputMin) / 20;
    rs.YieldMin = _.min(YieldSeriesData);
    rs.YieldMax = _.max(YieldSeriesData);
    rs.Yieldnterval = (rs.YieldMax - rs.YieldMin) / 20;
    rs.xAxisData = xAxisData;
    rs.OutputSeriesData = OutputSeriesData;
    rs.YieldSeriesData = YieldSeriesData;
    return rs;
}
router.get("/yield", async (req: express.Request, res: express.Response, next) => {
    var rs = await loadYieldDatas(req);
    res.send(rs);
});

export = router;


