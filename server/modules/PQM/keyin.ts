import * as express from "express";
import * as myutil from "../../common/util";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
var ObjectID = require("mongodb").ObjectID;

var tableName = "pqm.keyin";
//#region 增删改查
var items = db.collection(tableName);

router.get("/:templateCode", async (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    var templateCode = req.params.templateCode;

    var data = await items.find({ vendorCode: user.vendorCode, templateCode: templateCode }, {
        "pn": 1,
        "status": 1,
        "parent": 1
    }).sort({ "created": -1 }).toArrayAsync();

    _.each(data, row => {
        row.id = row._id;
        delete row._id;
    });

    data = myutil.Tree(data);
    res.send(data);
});

router.get("/process/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.params.id;
    var user = req.session["user"];
    var row = await items.findOneAsync({ _id: new ObjectID(id), vendorCode: user.vendorCode });

    row.id = row._id;
    row.process = myutil.Tree(row.process);
    delete row._id;

    res.send(row);
});
router.post("/:templateCode/", async (req: express.Request, res: express.Response, next) => {
    var templateCode = req.params.templateCode;
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var user = req.session["user"];
    obj.vendorCode = user.vendorCode;
    obj.vendorName = user.vendorName;
    obj.templateCode = templateCode;
    var templates = db.collection("pqm.templates");
    var template = await templates.findOneAsync({ vendorCode: user.vendorCode, templateCode: templateCode });
    template.process = template.process || [];
    template.processDetail = template.processDetail || [];

    obj.process = _.filter(template.process, (p: any) => p.processName).map((p: any) => { p.status = "待键入"; return p; });
    obj.processDetail = template.processDetail;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:templateCode/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:templateCode/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#endregion
router.put("/:pnid/detail/:id", async (req: express.Request, res: express.Response, next) => {
    var pnid = req.params.pnid;
    var id = req.params.id;
    var user = req.session["user"];
    var obj = req.body;
    obj.updated = new Date();
    if (obj.image)
        obj.image = JSON.parse(obj.image);
    delete obj.ch;
    obj.id = parseInt(id);
    var record = await items.updateOneAsync({
        _id: new ObjectID(pnid),
        vendorCode: user.vendorCode,
        "processDetail.id": parseInt(id)
    }, { $set: { "processDetail.$.Value": obj.Value, "processDetail.$.image": obj.image } });
    res.send({});
});
router.put("/:pnid/detail/:processid/ProcessParameter/:equipment/:time", async (req: express.Request, res: express.Response, next) => {
    var pnid = req.params.pnid;
    var processid = req.params.processid;
    var equipment = req.params.equipment;
    var time = req.params.time;
    var user = req.session["user"];
    var obj = req.body;
    if (obj.image)
        obj.image = JSON.parse(obj.image);
    var keyin = await db.collection("pqm.keyin").findOneAsync({ _id: new ObjectID(pnid)});
    var template: any = _.find(keyin.processDetail, { processId:  parseInt(processid),category:"工艺参数",Name: obj.Name});
    var insertobj = {
        pn: pnid,
        processid: processid,
        vendorCode: user.vendorCode,
        time: new Date(time),
        timestamp: new Date(),
        user: user.userName,
        equipment: equipment,
        parameter: obj.Name,
        value: parseFloat(obj.Value),
        image: obj.image,
        productName: obj.productName,
        productNumber: obj.productNumber,
        workorder: obj.workorder,
        process: obj.process,
        partNumber: obj.partNumber,
        uvalue: parseFloat(template.UpValue),
        nvalue: parseFloat(template.NominalValue),
        lvalue: parseFloat(template.LowValue),
        PPAP: parseFloat(template.PPAP),
        tuValue: parseFloat(template.triggerUpValue),
        tlValue: parseFloat(template.triggerLowValue)
    };
    var record = await db.collection("pqm.ProcessParameter").insertAsync(insertobj);
    res.send({});
});
router.get("/:pnid/detail/:processid/ProcessParameter/:name", async (req: express.Request, res: express.Response, next) => {
    var pnid = req.params.pnid;
    var processid = req.params.processid;
    var name = req.params.name;
    var user = req.session["user"];
    var data = await db.collection("pqm.ProcessParameter").find({
        pn: pnid,
        processid: processid,
        vendorCode: user.vendorCode,
        parameter: name
    }, { equipment: 1, value: 1, time: 1, user: 1 }).toArrayAsync();
    _.each(data, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(data);
});
router.put("/:pnid/process/:id", async (req: express.Request, res: express.Response, next) => {
    var pnid = req.params.pnid;
    var id = req.params.id;
    var user = req.session["user"];
    var obj = req.body;
    if (obj.form)
        obj.form = JSON.parse(obj.form);
    delete obj.ch;
    obj.id = parseInt(id);
    var record = await items.updateOneAsync({
        _id: new ObjectID(pnid),
        vendorCode: user.vendorCode,
        "process.id": parseInt(id)
    }, { $set: { "process.$": obj } });
    res.send({});
});
export = router;
