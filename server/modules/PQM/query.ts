import * as express from "express";
import * as myutil from "../../common/util";
import db = require("../../common/db");
import _ = require("lodash");
import moment = require("moment");
var xlsx = require('node-xlsx');
var router = express.Router();
import * as plugins from "../../common/plugins";
var ObjectID = require("mongodb").ObjectID;


async function loadProductionDatas(req: express.Request) {
    var productName = req.query.productName;
    var productNumber = req.query.productNumber;
    var workorder = req.query.workorder;
    var partNumber = req.query.partNumber;
    var hastime = req.query.beginTime != "null";
    hastime = hastime && req.query.endTime != "null";
    var beginTime, endTime;
    if (hastime) {
        beginTime = moment(new Date(req.query.beginTime));
        endTime = moment(new Date(req.query.endTime)).add(1, "day");
    }


    var user = req.session["user"];
    var templates = db.collection("pqm.templates");
    var keyins = db.collection("pqm.keyin");
    var mytemplates = await templates.find({ vendorCode: user.vendorCode, productName: productName, productNumber: productNumber }).toArrayAsync();
    var templateCodes = mytemplates.map(t => { return t.templateCode });
    var mykeyins = await keyins.find({ vendorCode: user.vendorCode, templateCode: { $in: templateCodes } }).toArrayAsync();


    var rs = [];

    mykeyins.forEach(keyin => {
        if (keyin.parent == 0) return;
        var parent: any = _.find(mykeyins, { _id: new ObjectID(keyin.parent) });
        if (!parent) return;
        if (workorder) {
            var r = new RegExp(workorder, "i")
            if (!r.test(parent.pn)) return;
        }
        if (partNumber) {
            var r = new RegExp(partNumber, "i")
            if (!r.test(keyin.pn)) return;
        }

        var processlst = keyin.process;
        processlst.forEach(process => {
            if (hastime) {
                if (!process.form) return;
                var rowbeginTime = moment(process.form.beginTime);
                var rowendTime = moment(process.form.endTime);

                if (beginTime > rowbeginTime || endTime < rowendTime) return;
            }
            var row: any = {
                productName: productName,
                productNumber: productNumber,
                workorder: parent.pn,
                partNumber: keyin.pn,
                process: process.processName,

            }
            if (process.form) {
                row.beginTime = process.form.beginTime;
                row.endTime = process.form.endTime;
                row.incount = process.form.incount;
                row.outcount = process.form.outcount;
                row.badcount = process.form.badcount;
                row.person = process.form.person;
                row.equipment = process.form.equipmentNumber;
                row.mould = process.form.mouldNumber;
            }
            rs.push(row);
        });
    });
    return rs;
}
async function loadProductionDatasByWorkorderId(req: express.Request,workorderid) {
   
    var user = req.session["user"];
    var templates = db.collection("pqm.templates");
    var keyins = db.collection("pqm.keyin");
    var parent = await keyins.findByIdAsync(workorderid);
    var template =await templates.findOneAsync({templateCode:parent.templateCode});
    var mykeyins = await keyins.find({parent:workorderid}).toArrayAsync();
    var rs = [];
    mykeyins.forEach(keyin => {
        var processlst = keyin.process;
        processlst.forEach(process => {
            var row: any = {
                productName: template.productName,
                productNumber: template.productNumber,
                workorder: parent.pn,
                partNumber: keyin.pn,
                process: process.processName,

            }
            if (process.form) {
                row.beginTime = process.form.beginTime;
                row.endTime = process.form.endTime;
                row.incount = process.form.incount;
                row.outcount = process.form.outcount;
                row.badcount = process.form.badcount;
                row.person = process.form.person;
                row.equipment = process.form.equipmentNumber;
                row.mould = process.form.mouldNumber;
            }
            rs.push(row);
        });
    });
    return rs;
}
async function loadProcessParametersDatas(req: express.Request) {
    var productName = req.query.productName;
    var productNumber = req.query.productNumber;
    var workorder = req.query.workorder;
    var partNumber = req.query.partNumber;
    var equipmentType = req.query.equipmentType;
    var equipment = req.query.equipment;
    var mode = req.query.mode;

    var hastime = req.query.beginTime != "null";
    hastime = hastime && req.query.endTime != "null";
    var beginTime, endTime;
    if (hastime) {
        beginTime = moment(new Date(req.query.beginTime));
        endTime = moment(new Date(req.query.endTime)).add(1, "day");
    }
    var user = req.session["user"];
    var rs = [];
    var filter: any = { vendorCode: user.vendorCode };
    if (mode == "equipmentQuery") {

        if (equipment) {
            filter.equipment = equipment;
        } else {
            var equipments = await db.collection("equipments").find({ equipmentType: equipmentType, vendorCode: user.vendorCode }).toArrayAsync();
            filter.equipment = { $in: equipments.map(e => { return e.equipmentCode }) };
        }
        if (hastime) {
            filter.time = {
                $gte: beginTime.toDate(),
                $lt: endTime.toDate()
            }
        }

    } else {
        filter.productName = productName;
        filter.productNumber = productNumber;
        if (workorder) {
            var r = new RegExp(workorder, "i")
            filter.workorder = r;
        }
        if (partNumber) {
            var r = new RegExp(partNumber, "i")
            filter.partNumber = r;
        }
    }

    var Parameters = await db.collection("pqm.ProcessParameter").find(filter).toArrayAsync();
    for (var index = 0; index < Parameters.length; index++) {
        var element = Parameters[index];
        //await db.collection("pqm.keyin").findOneAsync({ vendorCode: user.vendorCode,pn:element.pn})
        var row: any = {
            productName: element.productName,
            productNumber: element.productNumber,
            workorder: element.workorder,
            partNumber: element.partNumber,
            process: element.process,
            equipment: element.equipment,
            parameter: element.parameter,
            nvalue: element.nvalue,
            value: element.value,
            user: element.user,
            time: element.time
        }
        rs.push(row);
    }
    return rs;
}
async function loadQualityDatas(req: express.Request) {
    var productName = req.query.productName;
    var productNumber = req.query.productNumber;
    var workorder = req.query.workorder;
    var partNumber = req.query.partNumber;
    // var hastime = req.query.beginTime!="null";
    // hastime = hastime && req.query.endTime!="null";
    // var beginTime, endTime;
    // if (hastime) {
    //     beginTime = moment(req.query.beginTime);
    //     endTime = moment(req.query.endTime).add(1,"day");
    // }


    var user = req.session["user"];
    var templates = db.collection("pqm.templates");
    var keyins = db.collection("pqm.keyin");
    var mytemplates = await templates.find({ vendorCode: user.vendorCode, productName: productName, productNumber: productNumber }).toArrayAsync();
    var templateCodes = mytemplates.map(t => { return t.templateCode });
    var mykeyins = await keyins.find({ vendorCode: user.vendorCode, templateCode: { $in: templateCodes } }).toArrayAsync();


    var rs = [];

    mykeyins.forEach(keyin => {
        if (keyin.parent == 0) return;
        var parent: any = _.find(mykeyins, { _id: new ObjectID(keyin.parent) });
        if (!parent) return;
        if (workorder) {
            var r = new RegExp(workorder, "i")
            if (!r.test(parent.pn)) return;
        }
        if (partNumber) {
            var r = new RegExp(partNumber, "i")
            if (!r.test(keyin.pn)) return;
        }

        var processlst = keyin.process;
        processlst.forEach(process => {
            // if (hastime) {
            //     if (!process.form) return;
            //     var rowbeginTime = moment(process.form.beginTime);
            //     var rowendTime = moment(process.form.endTime);

            //     if(beginTime>rowbeginTime || endTime<rowendTime) return;
            // }
            var row: any = {
                productName: productName,
                productNumber: productNumber,
                workorder: parent.pn,
                partNumber: keyin.pn,
                process: process.processName,
            }
            var details = _.filter(keyin.processDetail, { processId: process.id });
            for (var index = 0; index < details.length; index++) {
                var element: any = details[index];
                var unit = (element.unit === undefined ? "" : element.unit);
                var Value = (element.Value === undefined ? "" : element.Value);
                var NominalValue = (element.NominalValue === undefined ? "" : element.NominalValue);
                //row[element.Name + "_u"] = element.UpValue||""+" "+element.unit||"";
                row[element.Name + "_n"] = NominalValue + " " + unit;
                //row[element.Name + "_l"] = element.LowValue||""+" "+element.unit||"";
                //row[element.Name + "_p"] = element.PPAP||""+" "+element.unit||"";
                row[element.Name] = Value + " " + unit;
            }
            rs.push(row);
        });
    });
    return rs;
}
async function loadQualityColumns(req: express.Request) {
    var productName = req.query.productName;
    var productNumber = req.query.productNumber;
    var workorder = req.query.workorder;
    var partNumber = req.query.partNumber;

    var user = req.session["user"];
    var templates = db.collection("pqm.templates");
    var keyins = db.collection("pqm.keyin");
    var mytemplates = await templates.find({ vendorCode: user.vendorCode, productName: productName, productNumber: productNumber }).toArrayAsync();
    var templateCodes = mytemplates.map(t => { return t.templateCode });
    var mykeyins = await keyins.find({ vendorCode: user.vendorCode, templateCode: { $in: templateCodes } }).toArrayAsync();


    var columns: any = [{ id: "workorder", header: "工单号", width: 150, editor: "text" },
        { id: "partNumber", header: "批次号", width: 150, editor: "text" },
          { id: "process", header: "工序", width: 150, editor: "text" },
        ];
    var exists = [];
    mykeyins.forEach(keyin => {
        if (keyin.parent == 0) return;
        var parent: any = _.find(mykeyins, { _id: new ObjectID(keyin.parent) });
        if (!parent) return;
        if (workorder) {
            var r = new RegExp(workorder, "i")
            if (!r.test(parent.pn)) return;
        }
        if (partNumber) {
            var r = new RegExp(partNumber, "i")
            if (!r.test(keyin.pn)) return;
        }

        if (exists.indexOf(keyin.templateCode) > 0) return;
        var template: any = _.find(mytemplates, { templateCode: keyin.templateCode });
        var details = _.filter(template.processDetail, { category: "品质检验" });
        for (var index = 0; index < details.length; index++) {
            var element: any = details[index];
            if (_.find(columns, { id: element.Name })) continue;
            var column5 = { id: element.Name, header: element.Name, width: 150 };
            columns.push(column5);
            // var column1 = { id: element.Name + "_u", header: element.Name + "上限", width: 150 };
            // columns.push(column1);
            var column2 = { id: element.Name + "_n", header: element.Name + "标准", width: 150 };
            columns.push(column2);
            // var column3 = { id: element.Name + "_l", header: element.Name + "下限", width: 150 };
            // columns.push(column3);
            // var column4 = { id: element.Name + "_p", header: element.Name + "PPAP", width: 150 };
            // columns.push(column4);

        }
        exists.push(keyin.templateCode);
    });
    return columns;
}

router.get("/production", async (req: express.Request, res: express.Response, next) => {
    var rs = await loadProductionDatas(req);
    res.send(rs);
});

router.get("/production/export", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var rs = await loadProductionDatas(req);
    var data = [];
    data.push(["产品名称", "产品型号", "工单号", "批次号", "工序", "开始时间", "结束时间", "投入", "产出", "不良数", "作业员", "设备", "模具"]);

    rs.forEach(row => {
        data.push(_.values(row));
    });

    var buffer = xlsx.build([{ name: "production", data: data }]);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats');
    res.setHeader("Content-Disposition", "attachment; filename=" + encodeURI(user.vendorName + ".xlsx"));
    res.end(buffer);

});
router.get("/production/:workorderid", async (req: express.Request, res: express.Response, next) => {
    var workorderid=req.params.workorderid;
    var rs = await loadProductionDatasByWorkorderId(req,workorderid);
    res.send(rs);
});
router.get("/quality", async (req: express.Request, res: express.Response, next) => {
    var rs = await loadQualityDatas(req);
    res.send(rs);
});
router.get("/quality/columns", async (req: express.Request, res: express.Response, next) => {
    var columns = await loadQualityColumns(req);

    res.send(columns);
});
router.get("/quality/export", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var columns = await loadQualityColumns(req);
    var rs = await loadQualityDatas(req);
    var data = [];
    var headers = columns.map(c => { return c.header; });
    data.push(headers);
    rs.forEach(row => {
        var newrow = [];
        columns.forEach(col => {
            newrow.push(row[col.id]);
        });
        data.push(newrow);
    });

    var buffer = xlsx.build([{ name: "quality", data: data }]);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats');
    res.setHeader("Content-Disposition", "attachment; filename=" + encodeURI(user.vendorName + ".xlsx"));
    res.end(buffer);

});

router.get("/processparameters", async (req: express.Request, res: express.Response, next) => {
    var rs = await loadProcessParametersDatas(req);
    res.send(rs);
});
router.get("/processparameters/export", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var rs = await loadProcessParametersDatas(req);
    var data = [];
    data.push(["产品名称", "产品型号", "工单号", "批次号", "工序", "设备", "参数", "标准值","实际值", "用户", "时间"]);
    rs.forEach(row => {
        data.push(_.values(row));
    });

    var buffer = xlsx.build([{ name: "ProcessParameters", data: data }]);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats');
    res.setHeader("Content-Disposition", "attachment; filename=" + encodeURI(user.vendorName + ".xlsx"));
    res.end(buffer);
});

export = router;


