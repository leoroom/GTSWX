import * as express from "express";
import * as myutil from "../../common/util";
import db = require("../../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../../common/plugins";
var ObjectID = require("mongodb").ObjectID;

var tableName = "pqm.eAudit";
//#region 增删改查
var items = db.collection(tableName);

router.get("/:templateCode/pager", (req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    var templateCode = req.params.templateCode;
    filter.vendorCode = user.vendorCode;
    filter.templateCode = templateCode;
    req.query.and = filter;
    next();
}, plugins.webix_all(tableName, true, {
    "pn": 1,
    "status": 1
}));

router.get("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.params.id;
    var user = req.session["user"];
    var row = await items.findOneAsync({ _id: new ObjectID(id), vendorCode: user.vendorCode });

    row.id = row._id;
    row.process = myutil.Tree(row.process);
    delete row._id;

    res.send(row);
});
router.post("/:templateCode/", async (req: express.Request, res: express.Response, next) => {
    var templateCode = req.params.templateCode;
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var user = req.session["user"];
    obj.vendorCode = user.vendorCode;
    obj.vendorName = user.vendorName;
    obj.templateCode = templateCode;
    var templates = db.collection("pqm.eAudittemplate");
    var template = await templates.findOneAsync({ vendorCode: user.vendorCode, templateCode: templateCode });
    template.process = template.process || [];
    template.processDetail = template.processDetail || [];

    obj.process = _.filter(template.process, (p: any) => p.processCategorys).map((p: any) => { p.status = "待键入"; return p; });
    obj.processDetail = template.processDetail;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:templateCode/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:templateCode/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#endregion
router.put("/:pnid/detail/:id", async (req: express.Request, res: express.Response, next) => {
    var pnid = req.params.pnid;
    var id = req.params.id;
    var user = req.session["user"];
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.$css;
    obj.id = parseInt(id);
    var record = await items.updateOneAsync({
        _id: new ObjectID(pnid),
        vendorCode: user.vendorCode,
        "processDetail.id": parseInt(id)
    }, { $set: { "processDetail.$.Value": obj.Value,
                 "processDetail.$.status": obj.status,
                 "processDetail.$.remark": obj.remark, } });
    res.send({});
});
router.put("/:pnid/process/:id", async (req: express.Request, res: express.Response, next) => {
    var pnid = req.params.pnid;
    var id = req.params.id;
    var user = req.session["user"];
    var obj = req.body;
    if (obj.form)
        obj.form = JSON.parse(obj.form);
    delete obj.ch;
      delete obj.$css;
    obj.id = parseInt(id);
    var record = await items.updateOneAsync({
        _id: new ObjectID(pnid),
        vendorCode: user.vendorCode,
        "process.id": parseInt(id)
    }, { $set: { "process.$": obj } });
    res.send({});
});
export = router;
