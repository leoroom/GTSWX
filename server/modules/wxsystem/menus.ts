import * as express from "express";
var WechatAPI = require("wechat-api");
import * as fs from "fs";

var router = express.Router();
var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));
var menu_config = wxconfig.wx.wx_menu;
var app_id = wxconfig.wx.appID;
var app_secret = wxconfig.wx.appsecret;

/**
 * 获取菜单列表
 */
router.get('/', async (req, res, next) => {
    var api = new WechatAPI(app_id,app_secret);
    api.getMenu(function(err,result) {
        if (err) {
             res.send(err);
            console.log(err);
        }else{
             res.send(result);
        }
    });
});

/**
 * 获取自定义菜单配置信息
 */
router.get('/getMenuConfig',async(req,res,next)=>{
     var api = new WechatAPI(app_id,app_secret);
    api.getMenuConfig(function(err,result) {
        if (err) {
            res.send(err);
            console.log(err);
        }else{
            res.send(result);
        }
    });
});
/**
 * 生成菜单
 */
router.post('/createMenu', async (req, res, next) => {
    var menuData = req.body;
    var menus;
    if (!menuData) {
        menus = menu_config;
    }else{
        menus = JSON.parse(menuData);
    }
    var api = new WechatAPI(app_id, app_secret);
    api.createMenu(menus, function (err, result) {
        if (err) {
            res.send(err);
            console.log(err);
        } else {
            res.send(result);
            console.log(result);
        }
    });

});
/**
 * 删除自定义菜单
 */
router.post('/removeMenu',async(req,res,nexrt)=>{
    var api = new WechatAPI(app_id, app_secret);
    api.removeMenu(function (err, result) {
        if (err) {
            res.send(err);
            console.log(err);
        } else {
            res.send(result);
            console.log(result);
        }
    });
});
export = router;