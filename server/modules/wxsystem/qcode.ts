import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
var WechatAPI = require("wechat-api");
import * as fs from "fs";

var router = express.Router();
var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));

var app_id = wxconfig.wx.appID;
var app_secret = wxconfig.wx.appsecret;

/**
 *默认创建临时二维码
 * 带场景id
 */
router.get('/:sceneId',async(req,res,next)=>{
    var sceneId = req.param("sceneId");
    if (!sceneId) {
        res.end(501);
    }
    var api = new WechatAPI(app_id,app_secret);
    var expire = 604800;//过期时间，单位秒。最大不超过604800（即7天
    await api.createTmpQRCode(sceneId,expire,function(err,result) {
       if (err) {
           res.send(err);
       } else{
           res.send(result);
       }
    });
});
/**
 * 创建临时服务号二维码
 * 参数1：sceneId：number，场景ID。数字ID不能大于100000，字符串ID长度限制为1到64
 * 参数2：expire：number，过期时间，单位秒。最大不超过604800（即7天
 * result:{
 "ticket":"gQG28DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0FuWC1DNmZuVEhvMVp4NDNMRnNRAAIEesLvUQMECAcAAA==",
 "expire_seconds":1800
}
通过ticket票据生成二维码图片
 */
router.get('/createTmpQRCode/:sceneId',async(req,res,next)=>{
    var sceneId = req.param("sceneId");
    if (!sceneId) {
        res.end(501);
    }
    var api = new WechatAPI(app_id,app_secret);
    var expire = 604800;//过期时间，单位秒。最大不超过604800（即7天
    await api.createTmpQRCode(sceneId,expire,function(err,result) {
       if (err) {
           res.send(err);
       } else{
           res.send(result);
       }
    });
});
/**
 * 生成永久二维码
 * 参数1：sceneId(Number,String)场景ID。数字ID不能大于100000，字符串ID长度限制为1到64
 * result：
 * {
 "ticket":"gQG28DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0FuWC1DNmZuVEhvMVp4NDNMRnNRAAIEesLvUQMECAcAAA=="
}
 */
router.get('/createLimitQRCode/:sceneId',async(req,res,next)=>{
    var sceneId = req.param("sceneId");
    if (!sceneId) {
        res.end(501);
    }
    var api = new WechatAPI(app_id,app_secret);
    
    await api.createLimitQRCode(sceneId,function(err,result) {
       if (err) {
           res.send(err);
       } else{
           res.send(result);
       }
    });
});
/**
 * 通过票据获取二维码图片地址
 * 参数：ticket:string,调用生成二维码方法获取到ticket
 * result:String 显示二维码的URL地址，通过img标签可以显示出来
 */
router.get('/getQRCodeUrl/:ticket',async(req,res,next)=>{
    var ticket = req.param('ticket');
    if (!ticket) {
        res.end(404);
    }
    var api = new WechatAPI(app_id,app_secret);
    var qrcodeUrl = await api.showQRCodeURL(ticket);
    res.send(qrcodeUrl);
});
export = router;