import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
import * as fs from "fs";
var WechatAPI = require("wechat-api");

var router = express.Router();
var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));

var app_id = wxconfig.wx.appID;
var app_secret = wxconfig.wx.appsecret;

/**
 * 获取服务号所有关注者，一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
 * result:{
 "total":2,
 "count":2,
 "data":{
   "openid":["","OPENID1","OPENID2"]
 },
 "next_openid":"NEXT_OPENID"
}
 */
router.get('/', async (req, res, next) => {
    var api = new WechatAPI(app_id, app_secret);
    await api.getFollowers(function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

/**
 * 获取服务号下一个10000关注者
 * 参数：next_openid，一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
 * result:{
 "total":2,
 "count":2,
 "data":{
   "openid":["","OPENID1","OPENID2"]
 },
 "next_openid":"NEXT_OPENID"
}
 */
router.get('/:nextopenid', async (req, res, next) => {
    var api = new WechatAPI(app_id, app_secret);
    var next_openid = req.param("nextopenid");
    if (!next_openid) {
        res.end(404, "next_openid has invalid value;");
    }
    await api.getFollowers(next_openid, function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

/**
 * 通过openid获取用户信息，可以设置lang，其中zh_CN 简体，zh_TW 繁体，en 英语。默认为en
 * 参数：openid
 * result：{
 "subscribe": 1,
 "openid": "o6_bmjrPTlm6_2sgVt7hMZOPfL2M",
 "nickname": "Band",
 "sex": 1,
 "language": "zh_CN",
 "city": "广州",
 "province": "广东",
 "country": "中国",
 "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
 "subscribe_time": 1382694957
}
 */
router.get('/getUserInfo/:openid', async (req, res, next) => {
    var api = new WechatAPI(app_id, app_secret);
    var langData = req.body;
    var lang = '';
    var openid = req.param("openid");
    if (langData === 'zh_CN' || langData === 'en' || langData === 'zh_TW') {
        lang = langData;
    } else {
        lang = "en";
    }
    var queryParam = lang == '' ? openid : { openid: openid, lang: lang };

    await api.getUser(queryParam, function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

/**
 * 通过openid获取用户信息，可以设置lang，其中zh_CN 简体，zh_TW 繁体，en 英语。默认为中文
 * 参数：openid
 * result：{
 "subscribe": 1,
 "openid": "o6_bmjrPTlm6_2sgVt7hMZOPfL2M",
 "nickname": "Band",
 "sex": 1,
 "language": "zh_CN",
 "city": "广州",
 "province": "广东",
 "country": "中国",
 "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
 "subscribe_time": 1382694957
}
 */
router.get('/getUserInfoByZH/:openid', async (req, res, next) => {
    var api = new WechatAPI(app_id, app_secret);
    var openid = req.param("openid");
    var queryParam = { openid: openid, lang: 'zh_CN' };
    await api.getUser(queryParam, function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});
/**
 * 批量获取用户信息，
 * 参数：openid数组['openid1', 'openid2']
 * result:
 * {
  "user_info_list": [{
    "subscribe": 1,
    "openid": "otvxTs4dckWG7imySrJd6jSi0CWE",
    "nickname": "iWithery",
    "sex": 1,
    "language": "zh_CN",
    "city": "Jieyang",
    "province": "Guangdong",
    "country": "China",
    "headimgurl": "http://wx.qlogo.cn/mmopen/xbIQx1GRqdvyqkMMhEaGOX802l1CyqMJNgUzKP8MeAeHFicRDSnZH7FY4XB7p8XHXIf6uJA2SCunTPicGKezDC4saKISzRj3nz/0",
    "subscribe_time": 1434093047,
    "unionid": "oR5GjjgEhCMJFyzaVZdrxZ2zRRF4",
    "remark": "",
    "groupid": 0
  }, {
    "subscribe": 0,
    "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg",
    "unionid": "oR5GjjjrbqBZbrnPwwmSxFukE41U",
  }]
}
 */
router.post('/batchGetUsers', async (req, res, next) => {
    var api = new WechatAPI(app_id, app_secret);
    var openidData = req.body.openid;
    var openid;
    if (!openidData) {
        res.end(404,"has invaid openid.");
    }
    openid = JSON.parse(openidData);

    await api.getUser(openid, function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});
export = router;