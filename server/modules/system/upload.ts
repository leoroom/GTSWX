import * as express from "express";
import * as db from "../../common/db";
import * as _ from "lodash";
import * as moment from "moment";
import * as fs from 'fs';
import * as path from 'path';
var formidable = require("formidable");
var router = express.Router();

var RootPath = process.env.RootPath;
var TempPath = process.env.TempPath;
var PicPath = process.env.PicPath;
var DocPath = process.env.DocPath;

router.get("/temp/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    res.sendFile(RootPath + TempPath + id);
});
router.get("/imgs/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    res.sendFile(RootPath + PicPath + id);
});
router.get("/docs/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    res.sendFile(RootPath + DocPath + id);
});
router.post("/temp", async (req: express.Request, res: express.Response, next) => {

    var form = new formidable.IncomingForm();
    form.uploadDir = RootPath + TempPath;
    form.encoding = 'utf-8';
    form.keepExtensions = true;
    var newpath, newname;
    form.on('file', function (name, file) {
        console.log("file.path:" + file.path);
        var filename = file.name.substring(0, file.name.lastIndexOf("."));
        var ext = file.name.substr(file.name.lastIndexOf("."));
        newname = filename + ext;
        newpath = "/" + TempPath + newname;
        var destPath = RootPath + newpath;
        var fileindex = 0;

        while (fs.existsSync(destPath)) {
            fileindex++;
            newname = filename + "(" + fileindex + ")" + ext;
            newpath = TempPath + newname;
            destPath = RootPath + newpath;
        }
        console.log("destPath:" + destPath);
        fs.renameSync(file.path, destPath);

    });
    form.on('end', function () {
        res.send({ status: "server", sname: newpath, name: newname });
    });
    form.on('error', function (err) {
        console.log("error:" + err);
        res.send({ status: "error" });
    });
    form.parse(req);

});
export = router;