﻿import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
import util = require("../../common/util");
var router = express.Router();

//#region 增删改查
var items = db.collection("menus");
router.get("/", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    var treeData = util.Tree(datas, {});
    res.send(treeData);
});
router.post("/", async (req: express.Request, res: express.Response, next)=> {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});
//#endregion

//#region 用户角色关系
var roleusers = db.collection("roleUsers");
var rolemenus = db.collection("roleMenus");
var roles = db.collection("roles");
var ObjectId = require("mongodb").ObjectId;
router.get("/authTree", async (req: express.Request, res: express.Response, next)=> {
    var user = req.session["user"];
    var isadmin = false;
    var userroledatas = await roleusers.find({ user: user.id }).toArrayAsync();
    var roleids = userroledatas.map(x => new ObjectId(x.role));
    var roledatas = await roles.find({ _id: { $in: roleids }, status: "1" }).toArrayAsync();
    var datas;
    if (_.find(roledatas, { roleName: "admin" })) {
        datas = await items.find({}, { sort: { seq: 1 } }).toArrayAsync();
    } else {
        roleids = roledatas.map(x => x._id.toString());
        var rolemenudatas = await rolemenus.find({ role: { $in: roleids } }).toArrayAsync();
        var menuids = _.uniq(rolemenudatas.map(p => new ObjectId(p.menu)));
        datas = await items.find({ _id: { $in: menuids } }, { sort: { seq: 1 } }).toArrayAsync();
    }
    _.each(datas, row => {
        row.id = row._id;
        row.value=row.name;
        row.icon=row.icon||"list";
        row.open=false;
        delete row._id;
    });
    var treeDatas = util.Tree(datas, {});
    // var rs = _.filter(treeDatas, (row: any) => {
    //     return row.parent == 0 && row.data;
    // });
    res.send(treeDatas);
});
//#endregion

export= router;
