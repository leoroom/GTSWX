﻿import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
import util = require("../../common/util");
import * as plugins from "../../common/plugins"
var router = express.Router();


// var webixData = require("webix-data"),
// webixDb = webixData(require("webix-mongo"), require("webix-rest")); 
// webixDb.db("mongodb://localhost/GTS");
// router.use("/",webixDb.crud("roles"), function(state, resolve) {
//     console.log(state);
// });


//#region 增删改查
var items = db.collection("roles");

router.get("/", async (req: express.Request, res: express.Response, next)=> {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});
router.get("/pager",plugins.webix_all("roles"));

router.post("/", async (req: express.Request, res: express.Response, next)=> {
    var obj = req.body;
    obj.created = new Date();
    delete obj.ch;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});
//#endregion
router.get("/company/pager",(req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.vendorCode = user.vendorCode;
    req.query.and = filter;
    next();
},plugins.webix_all("roles"));

router.post("/company/", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    obj.created = new Date();
    var user = req.session["user"];
    obj.vendorCode=user.vendorCode;
    obj.vendorName=user.vendorName;
    delete obj.ch;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/company/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    var user = req.session["user"];
    obj.vendorCode=user.vendorCode;
    obj.vendorName=user.vendorName;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/company/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});




//#region 用户角色关系
var roleusers = db.collection("roleUsers");
var users = db.collection("users");
router.get("/:id/users", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var selected = await roleusers.find({ role: id }).toArrayAsync();
    var datas = await users.find({ status: "1" }).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
        row.ch = (_.findIndex(selected, { user: row.id.toString() }) == -1) ? 0 : 1
    });
    res.send(datas);
});
router.post("/:id/users", async (req: express.Request, res: express.Response, next)=> {
    var roleid = req.param("id");
    var obj = req.body;
    var userid = obj.id;

    if (obj.ch == 1) {
        var record = await roleusers.insertAsync({ role: roleid, user: userid });;
    } else {
        var record = await roleusers.removeAsync({ role: roleid, user: userid });
    }
    res.send({});
});
//#endregion

//#region 角色菜单
var rolemenus = db.collection("roleMenus");
var menus = db.collection("menus");
router.get("/:id/menus", async (req: express.Request, res: express.Response, next)=> {
    var id = req.param("id");
    var selected = await rolemenus.find({ role: id }).toArrayAsync();
    var datas = await menus.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
        row.ch = (_.findIndex(selected, { menu: row.id.toString() }) == -1) ? 0 : 1
    });
    var treeDatas = util.Tree(datas, {});
    res.send(treeDatas);
});
router.post("/:id/menus", async (req: express.Request, res: express.Response, next)=> {
    var roleid = req.param("id");
    var obj = req.body;
    var menuid = obj.id;

    if (obj.ch == 1) {
        var record = await rolemenus.insertAsync({ role: roleid, menu: menuid });;
    } else {
        var record = await rolemenus.removeAsync({ role: roleid, menu: menuid });
    }
    res.send({});
});
//#endregion


export= router;
