﻿import * as express from "express";
import db = require("../../common/db");
import _ = require("lodash");
import * as plugins from "../../common/plugins"
var router = express.Router();

//#region 增删改查
var items = db.collection("users");
router.get("/", async (req: express.Request, res: express.Response, next) => {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
        delete row.passWord;
    });
    res.send(datas);
});
router.get("/pager",plugins.webix_all("users",false), (req: express.Request, res: express.Response, next) => {
    var senddata =req["senddata"];
    _.each(senddata.data, row => {
        delete row.passWord;
    });
    res.send(senddata);
});

router.post("/", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    obj.created = new Date();
    obj.passWord = "e10adc3949ba59abbe56e057f20f883e";//123456
    delete obj.ch;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});
//#endregion

router.get("/company/pager",(req: express.Request, res: express.Response, next) => {
    var filter = req.query.and || {};
    var user = req.session["user"];
    filter.vendorCode = user.vendorCode;
    req.query.and = filter;
    next();
}, plugins.webix_all("users",false), (req: express.Request, res: express.Response, next) => {
    var senddata =req["senddata"];
    _.each(senddata.data, row => {
        delete row.passWord;
    });
    res.send(senddata);
});
router.post("/company/", async (req: express.Request, res: express.Response, next) => {
    var obj = req.body;
    obj.created = new Date();
    obj.passWord = "e10adc3949ba59abbe56e057f20f883e";//123456
    var user = req.session["user"];
    obj.vendorCode=user.vendorCode;
    obj.vendorName=user.vendorName;
    obj.provider="company";
    delete obj.ch;
    var record = await items.insertAsync(obj);
    res.send({ newid: obj._id });
});
router.put("/company/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var obj = req.body;
    var user = req.session["user"];
    obj.vendorCode=user.vendorCode;
    obj.vendorName=user.vendorName;
    obj.provider="company";
    obj.updated = new Date();
    delete obj.ch;
    delete obj.id;
    var record = await items.updateByIdAsync(id, { $set: obj });
    res.send({});
});
router.delete("/company/:id", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var record = await items.removeByIdAsync(id);
    res.send({});
});

//#region 用户角色关系
var roleusers = db.collection("roleUsers");
var roles = db.collection("roles");
router.get("/:id/roles", async (req: express.Request, res: express.Response, next) => {
    var id = req.param("id");
    var selected = await roleusers.find({ user: id }).toArrayAsync();
    var datas = await roles.find({ status: "1" }).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
        row.ch = (_.findIndex(selected, { role: row.id.toString() }) == -1) ? 0 : 1
    });
    res.send(datas);
});
router.post("/:id/roles", async (req: express.Request, res: express.Response, next) => {
    var userid = req.param("id");
    var obj = req.body;
    var roleid = obj.id;

    if (obj.ch == 1) {
        var record = await roleusers.insertAsync({ role: roleid, user: userid });;   
    } else {
        var record = await roleusers.removeAsync({ role: roleid, user: userid });  
    }
    res.send({});
});
//#endregion

router.get("/combox", async (req: express.Request, res: express.Response, next) => {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
        delete row.passWord;
        row.value = row.userName;
        delete row.userName;
    });
    res.send(datas);
});
router.get("/my/combox", async (req: express.Request, res: express.Response, next) => {
    var user = req.session["user"];
    var datas = await items.find({vendorCode: user.vendorCode}).toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
        delete row.passWord;
        row.value = row.userName;
        delete row.userName;
    });
    res.send(datas);
});
export = router;
