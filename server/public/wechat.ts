import * as express from "express";
var WechatAPI = require("wechat-api");
var Wechat = require('wechat');
import * as fs from "fs";
import db = require("../common/db");
var ObjectId = require("mongodb").ObjectID;
var sha1 = require('sha1');

var router = express.Router();
var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));
var app_id = wxconfig.wx.appID;
var app_secret = wxconfig.wx.appsecret;
var app_token = wxconfig.wx.token;
var app_encodingAESKey = wxconfig.wx.encodingAESKey;

router.get('/',  async (req: express.Request, res: express.Response, next) => {
    // 获取校验参数
    wxconfig.wx.signature = req.query.signature,
        wxconfig.wx.timestamp = req.query.timestamp,
        wxconfig.wx.nonceStr = req.query.nonce,
        wxconfig.wx.echoStr = req.query.echostr;

    // 按照字典排序
    var array = [wxconfig.wx.token, wxconfig.wx.timestamp, wxconfig.wx.nonceStr];
    array.sort();

    // 连接
    var original = sha1(array.join(""));

    if (wxconfig.wx.signature == original) {
        res.end(wxconfig.wx.echoStr);
        console.log("Confirm and send echo back:" + wxconfig.wx.echoStr);
    } else {
        res.end("false");
        console.log("Failed!");
    }
});

router.get('/checkSignature', async (req: express.Request, res: express.Response, next) => {

    // 获取校验参数
    wxconfig.wx.signature = req.query.signature,
        wxconfig.wx.timestamp = req.query.timestamp,
        wxconfig.wx.nonceStr = req.query.nonce,
        wxconfig.wx.echoStr = req.query.echostr;

    // 按照字典排序
    var array = [wxconfig.wx.token, wxconfig.wx.timestamp, wxconfig.wx.nonceStr];
    array.sort();

    // 连接
    var original = sha1(array.join(""));

    if (wxconfig.wx.signature == original) {
        res.end(wxconfig.wx.echoStr);
        console.log("Confirm and send echo back:" + wxconfig.wx.echoStr);
    } else {
        res.end("false");
        console.log("Failed!");
    }
});

router.get('/initMenu', async (req: express.Request, res: express.Response, next) => {
    var wxconfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));
    var app_id = wxconfig.wx.appID;
    var app_secret = wxconfig.wx.appsecret;
    var app_token = wxconfig.wx.token;
    var menus = wxconfig.wx.wx_menu;

    var wechatapi = new WechatAPI(app_id, app_secret);
    wechatapi.createMenu(menus, function (err, result) {
        if (err) {
            console.log(err);
            res.end(err);
        } else {
            console.log(result);
            res.end(result.errmsg);
        }
    });

});

//在线下单页面
router.get('/sorting', async (req: express.Request, res: express.Response, next) => {
    res.redirect('./metroSignPage.html')
});

router.post('/',Wechat({ token: app_token, appid: app_id, encodingAESKey: app_encodingAESKey }, async (req, res, next) => {
    var signature = req.query.signature;
    var timestamp = req.query.timestamp;
    var nonceStr = req.query.nonce;
    var openid = req.query.openid;
    var msg = req.weixin;
    var userId = req.param("id");
    
    initMenus(res);
    wxconfig.wx.openid = openid;
    wxconfig.wx.nonceStr = nonceStr;
    wxconfig.wx.signature = signature;
    wxconfig.wx.timestamp = timestamp;

    // 按照字典排序
    var array = [app_token, timestamp, nonceStr];
    array.sort();

    var msgType = msg.MsgType;
    if (msgType == 'event' && msg.Event.toLowerCase() == "subscribe") {
        //var eventType = msgType.Event;
        //新用户关注订阅事件
        res.reply("终于等到您了，欢迎关注GTS服务号！");
       //subscribeEvent(res,openid,userId);

    
    } else if (msgType == 'event' && msg.Event.toLowerCase() == "view") {
        // 连接
        var original = sha1(array.join(""));
        if (signature == original) {
            var wechatapi = new WechatAPI(app_id, app_secret);
            if (openid) {
                await wechatapi.getUser({ openid: openid, lang: 'zh_CN' }, async (err, result) => {
                    var userInfo = result;
                    var users = db.collection("users");
                    var user = await users.findOneAsync({ openid: userInfo.openid });
                    console.log(userInfo);
                    if (user) {
                        next();
                    } else {
                        next();
                        res.redirect('./logon.html');
                    }

                });
            };
        } else {
            next();
        };
    }


}));

//主动推送消息
router.post('/sendTextMsg:/:toUser', async (req, res, next) => {
    var msgData = req.body;
    // if (!msgData) {
    //     return;
    // }
    // var msgModel = JSON.parse(msgData);
    //var toUser = msgModel.toUser;
    //var msgContent = msgModel.msgContent;
    var users = req.param("toUser");
    var wechatapi = new WechatAPI(app_id, app_secret);
    //  await wechatapi.getFollowers(function (err, result) {
    //     if (err) {
    //         //res.end(200,err);
    //     } else {
    //         users = JSON.parse(result);
    //         //res.send(result);
    //     }
    // });
    var toUser = 'oWYZnwwsSiVTfPjMvTSH2_qkBNnQ';
    var msgContent = '这是个群发测试消息，发票已开，请尽快打款。';
    wechatapi.massSendText(msgContent, true, function (err, result) {
        return err?err:result;
    });
    wechatapi.sendText(toUser, msgContent, function (err, result) {
        if (err) {
            console.log(err);
            res.end(err);
        } else {
            console.log(result);
            res.end(result.errmsg);
        }
    });
});

router.use('/',Wechat({ token: app_token, appid: app_id, encodingAESKey: app_encodingAESKey }, async (req, res, next) => {
    var signature = req.query.signature;
    var timestamp = req.query.timestamp;
    var nonceStr = req.query.nonce;
    var openid = req.query.openid;
    var msg = req.weixin;
    var userId = req.param("id");
    
    initMenus(res);
    wxconfig.wx.openid = openid;
    wxconfig.wx.nonceStr = nonceStr;
    wxconfig.wx.signature = signature;
    wxconfig.wx.timestamp = timestamp;

    // 按照字典排序
    var array = [app_token, timestamp, nonceStr];
    array.sort();

    var msgType = msg.MsgType;
    if (msgType == 'event' && msg.Event.toLowerCase() == "subscribe") {
        //var eventType = msgType.Event;
        //新用户关注订阅事件
        res.reply("终于等到您了，欢迎关注GTS服务号！");
       //subscribeEvent(res,openid,userId);
    
    }else if (msgType == 'event' && msg.Event.toLowerCase() == "view") {
        // 连接
        var original = sha1(array.join(""));
            var wechatapi = new WechatAPI(app_id, app_secret);
            if (openid) {
                await wechatapi.getUser({ openid: openid, lang: 'zh_CN' }, async (err, result) => {
                    var userInfo = result;
                    var users = db.collection("users");
                    var user = await users.findOneAsync({ openid: userInfo.openid });
                    console.log(userInfo);
                    if (user) {
                        next();
                    } else {
                        next();
                        res.redirect('./logon.html');
                    }

                });
            };
        } else {
            next();
        };
    



}));

var initMenus = function (res) {
    var menus = wxconfig.wx.wx_menu;
    var wechatapi = new WechatAPI(app_id, app_secret);
    wechatapi.createMenu(menus, function (err, result) {
        if (err) {
            console.log(err);
            res.end(err);
        } else {
            console.log(result);
            res.end(result.errmsg);
        }
    });
};

/**
 * 用户关注/订阅事件处理：根据带参数的用户ID，将openid绑定到当前关注的用户信息
 * 微信用户账号通过openid与平台用户账号进行绑定
 */
var subscribeEvent = async (res, openid, userId) => {
    var users = db.collection("users");
    var user = await users.findByIdAsync(userId);
    if(user){
        user.openid = openid;
        var record = await users.saveAsync(user);
        res.reply("终于等到您了，欢迎关注GTS服务号！");
    }else{
        res.end(200);
    }

}
export = router;