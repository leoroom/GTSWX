import * as express from "express";
var router = express.Router();
import db = require("../common/db");
import * as path from "path";
router.get("/", async (req: express.Request, res: express.Response) => {
    var host = req.headers['host'];
    var vendor;
    if (host) {
        var items = db.collection("vendors");
        vendor = await items.findOneAsync({ domainUrl: host });
    }
    var logoPath;
    if (vendor) {
        logoPath = path.join(__dirname, "../../", vendor.logoPath);

    } else {
        logoPath = path.join(__dirname, "../../", '/logos/logo.png');
    }
    res.sendFile(logoPath);
})

router.get("/home", async (req: express.Request, res: express.Response) => {
    var host = req.headers['host'];
    var vendor;
    if (host) {
        var items = db.collection("vendors");
        vendor = await items.findOneAsync({ domainUrl: host });
    }
    var logoPath;
    if (vendor) {
        logoPath = path.join(__dirname, "../../", vendor.logoHomePath);

    } else {
        logoPath = path.join(__dirname, "../../", '/logos/logo_home.png');
    }
    res.sendFile(logoPath);
})


export =router;