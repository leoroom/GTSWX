import * as express from "express";
import db = require("../common/db");
import _ = require("lodash");
var router = express.Router();
import * as plugins from "../common/plugins";
//#region 增删改查
var items = db.collection("pqm.temp");

router.get("/", async (req: express.Request, res: express.Response, next) => {
    var datas = await items.find().toArrayAsync();
    _.each(datas, row => {
        row.id = row._id;
        delete row._id;
    });
    res.send(datas);
});

router.post("/:id", async (req: express.Request, res: express.Response, next) => {
    var users= req.body;
    if(!users.data) return;
    var selected=JSON.parse(users.data);
    var id = req.param("id"); 
    var vendor = await items.findByIdAsync(id);
    selected.forEach(user => {
        _.find(vendor.users,{name:user.name})["sign"]=true;
    }); 
    vendor.status="已签到";
    var record = await items.saveAsync(vendor);
    res.send({status:true});
});

router.get("/clearAll", async (req: express.Request, res: express.Response, next) => {
    var record = await items.updateManyAsync({}, { $set:{status:"未签到"}});
    res.send({status:true});
});


export = router;
